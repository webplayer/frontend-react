import React from 'react'
import { connect } from 'react-redux'

const withAuthProps = Component =>
  connect(mapStateToProps)(
    class withAuthProps extends React.Component {
      render() {
        return <Component {...this.props} />
      }
    }
  )

const mapStateToProps = store => {
  return {
    username: store.authReducer.username,
    id: store.authReducer.id,
    isAppSignIn: store.authReducer.isAppSignIn,
    isVkSignIn: store.authReducer.isVkSignIn,
    backgroundImage: store.authReducer.backgroundImage,
  }
}

export default withAuthProps
