import React from 'react'
import { connect } from 'react-redux'

const withMobileSearch = Component =>
  connect(mapStateToProps)(
    class WithMobileSearch extends React.Component {
      render() {
        return <Component {...this.props} />
      }
    }
  )

const mapStateToProps = store => {
  return {
    mobileSearch: store.mobileSearchReducer.mobileSearch,
    signal: store.mobileSearchReducer.signal,
  }
}

export default withMobileSearch
