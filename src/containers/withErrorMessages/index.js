import React from 'react'
import { connect } from 'react-redux'

const withErrorMessages = Component =>
  connect(mapStateToProps)(
    class withErrorMessages extends React.Component {
      render() {
        return <Component {...this.props} />
      }
    }
  )

const mapStateToProps = store => {
  return {
    errorResponse: store.authReducer.errorResponse,
  }
}

export default withErrorMessages
