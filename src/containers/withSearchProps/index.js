import React from 'react'
import { connect } from 'react-redux'

const withSearchProps = Component =>
  connect(mapStateToProps)(
    class WithSearchProps extends React.Component {
      render() {
        return <Component {...this.props} />
      }
    }
  )

const mapStateToProps = store => {
  return {
    tempPlaylist: store.searchReducer.tempPlaylist,
    currentPlaylist: store.searchReducer.currentPlaylist,
    isFetching: store.searchReducer.isFetching,
    isPlaylistsFetching: store.searchReducer.isPlaylistsFetching,
    currentSearch: store.searchReducer.currentSearch,
    currentSearchPage: store.searchReducer.currentSearchPage,
    isFetchingNext: store.searchReducer.isFetchingNext,
    volume: store.searchReducer.volume,
    userPlaylists: store.searchReducer.userPlaylists,
    currentPlaylistId: store.searchReducer.currentPlaylistId,
    nextSearchUrl: store.searchReducer.nextSearchUrl,
    popularPage: store.searchReducer.popularPage,
    searchParams: store.searchReducer.searchParams,
    isSearchParams: store.searchReducer.isSearchParams,
    sources: store.searchReducer.sources,
  }
}

export default withSearchProps
