import React from 'react'
import { connect } from 'react-redux'

const withPlayerProps = Component =>
  connect(mapStateToProps)(
    class WithPlayerProps extends React.Component {
      render() {
        return <Component {...this.props} />
      }
    }
  )

const mapStateToProps = store => {
  return {
    currentAudio: store.playerReducer.currentAudio,
    isPlaying: store.playerReducer.isPlaying,
  }
}

export default withPlayerProps
