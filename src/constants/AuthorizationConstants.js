export const VK_AUTHORIZATION_HEADER = 'Авторизация VK'
export const VK_AUTHORIZATION_LOGIN_PLACEHOLDER = 'логин'
export const VK_AUTHORIZATION_PASSWORD_PLACEHOLDER = 'пароль'

export const APP_AUTHORIZATION_HEADER = 'Авторизация App'
export const APP_AUTHORIZATION_LOGIN_PLACEHOLDER = 'логин'
export const APP_AUTHORIZATION_PASSWORD_PLACEHOLDER = 'пароль'

export const LOGIN_BUTTON = 'Войти'
