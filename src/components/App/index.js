import React from 'react'

import './index.css'

import Footer from '../AppStructure/Footer/Footer'
import Content from '../AppStructure/Content/Content'
import { MDBCol, MDBContainer, MDBRow } from 'mdbreact'
import Header from '../AppStructure/Header/Header'
import withPlayerProps from '../../containers/withPlayerProps'
import { checkAuthRequest, setBackgroundImage } from '../../store/auth/actions'
import withAuthProps from '../../containers/withAuthProps'
import { getSearchParams } from '../../store/search/actions'

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      style: 'custom-navbar',
      prevScrollPos: window.pageYOffset,
    }

    if (!localStorage.getItem('backgroundImage')) {
      localStorage.setItem('backgroundImage', require('./back_3840x2400.webp'))
    }

    this.props.dispatch(
      setBackgroundImage(localStorage.getItem('backgroundImage'))
    )

    this.props.dispatch(checkAuthRequest())

    this.props.dispatch(getSearchParams())
    // let source = new Source('vk',null)
    // let promise = source.newSearch('nightcore')
    // promise.then(()=>{
    //   console.log('hello')
    // })
  }

  render() {
    return (
      <>
        <div
          style={{ backgroundImage: 'url(' + this.props.backgroundImage + ')' }}
          className={'bg-image'}
        />
        <MDBContainer fluid={true} className="App">
          <Header style={this.state.style} />
          {this.props.isAppSignIn && (
            <MDBRow center>
              <MDBCol size={12} md={8} lg={6} xl={4} className="Content">
                <Content />
              </MDBCol>
            </MDBRow>
          )}
          <Footer />
        </MDBContainer>
      </>
    )
  }
}

export default withAuthProps(withPlayerProps(App))
