import './index.css'

import React from 'react'
import Search from '../Search/Search'
import { MDBBtn, MDBCol, MDBContainer, MDBNavbar, MDBNavbarBrand, MDBRow } from 'mdbreact'
import withMobileSearch from '../../../containers/withMobileSearchProps'
import AppSettings from '../AppSettings/AppSettings'
import { NavLink } from 'react-router-dom'
import { setSourcesStatus } from '../../../store/search/actions'
import ProfileButtonGroup from '../ProfileButton/ProfileButton'
import withAuthProps from '../../../containers/withAuthProps'
import AuthButton from '../AuthButton/AuthButton'
import withSearchProps from '../../../containers/withSearchProps'

class Navigation extends React.Component {
  state = {
    isOpen: false,
    activeSourceParam: 'youtube',
  }

  toggleSource = event => {
    let targetSource = event.target.id
    this.props.dispatch(
      setSourcesStatus(
        this.props.sources.find(source => source.source === targetSource)
      )
    )
  }

  render() {
    return (
      <MDBNavbar fixed="top" className={'custom-navbar'}>
        <MDBRow between className={'no-gutters maxWidth'}>
          {!this.props.mobileSearch && (
            <MDBCol size={3} md={2} className={'custom-brand'}>
              <NavLink to={'/'}>
                <MDBNavbarBrand>
                  <strong className="white-text">VKTube</strong>
                </MDBNavbarBrand>
              </NavLink>
            </MDBCol>
          )}
          <MDBCol size={this.props.mobileSearch ? 12 : 6} md={8} lg={6} xl={4}>
            <Search history={this.props.history} match={this.props.match} />
          </MDBCol>
          {!this.props.mobileSearch && (
            <MDBCol size={3} md={2}>
              <MDBRow between className={'no-gutters'}>
                <AppSettings />
                {this.props.isAppSignIn ? (
                  <ProfileButtonGroup />
                ) : (
                  <AuthButton />
                )}
              </MDBRow>
            </MDBCol>
          )}
        </MDBRow>
        {this.props.isSearchParams && (
          <MDBContainer fluid className={'customSearchParams'}>
            <MDBRow center className={'maxWidth'}>
              <MDBCol className={'col-6 col-md-4 col-lg-3 col-xl-2'}>
                {this.props.sources.map(source => {
                  console.log(source)
                  return (
                    <MDBRow key={source.source}>
                      <MDBBtn
                        className={
                          source.status
                            ? 'source source-active'
                            : 'source source-disable'
                        }
                        gradient="blue"
                        rounded
                        id={source.source}
                        onClick={this.toggleSource}
                      >
                        {source.source}
                      </MDBBtn>
                    </MDBRow>
                  )
                })}
              </MDBCol>
              <MDBCol className={'col-6 col-md-4 col-lg-3 col-xl-2'}>
                {this.props.searchParams
                  .find(el => {
                    return el.source === this.state.activeSourceParam
                  })
                  .params.map(param => {
                    switch (param.type) {
                      case 'switch':
                        return (
                          <div
                            key={param.param}
                            className="custom-control custom-switch"
                          >
                            <input
                              type="checkbox"
                              className="custom-control-input"
                              id={param.param}
                            />
                            <label
                              className="custom-control-label"
                              htmlFor={param.param}
                            >
                              {param.name}
                            </label>
                          </div>
                        )
                      case 'select':
                        return (
                          <MDBRow>
                            <MDBBtn>{param.name}</MDBBtn>
                          </MDBRow>
                        )
                      default:
                        return <></>
                    }
                  })}
              </MDBCol>
            </MDBRow>
          </MDBContainer>
        )}
      </MDBNavbar>
    )
  }
}

export default withAuthProps(withMobileSearch(withSearchProps(Navigation)))
