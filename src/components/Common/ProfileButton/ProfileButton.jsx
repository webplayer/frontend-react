import './index.css'
import React from 'react'
import withAuthProps from '../../../containers/withAuthProps'
import { appSignOut, vkSignIn } from '../../../store/auth/actions'
import InputGroup from 'react-bootstrap/InputGroup'
import FormControl from 'react-bootstrap/FormControl'
import {
  LOGIN_BUTTON,
  VK_AUTHORIZATION_HEADER,
  VK_AUTHORIZATION_LOGIN_PLACEHOLDER,
  VK_AUTHORIZATION_PASSWORD_PLACEHOLDER,
} from '../../../constants/AuthorizationConstants'
import {
  MDBBtn,
  MDBCol,
  MDBContainer,
  MDBIcon,
  MDBModal,
  MDBModalBody,
  MDBModalFooter,
  MDBModalHeader,
  MDBRow,
} from 'mdbreact'

class ProfileButton extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      showApp: false,
      showVK: false,
      loginVK: '',
      passwordVK: '',
      loginApp: '',
      passwordApp: '',
      isVKPasswordChanged: false,
      isAppPasswordChanged: false,
    }
  }

  toggleApp = () => {
    if (this.props.isAppSignIn) {
      this.setState({ showApp: !this.state.showApp })
    }
  }

  toggleVK = () => {
    this.setState({
      showVK: !this.state.showVK,
      showApp: !this.state.showApp,
      loginVK: '',
      passwordVK: '',
    })
  }

  logout = () => {
    this.props.dispatch(appSignOut())
    this.toggleApp()
  }

  authorizeVK = () => {
    if (this.state.loginVK !== '' && this.state.passwordVK !== '') {
      this.props.dispatch(vkSignIn(this.state.loginVK, this.state.passwordVK))
    }
    this.toggleVK()
  }

  handleEnterVK = event => {
    if (event.key === 'Enter') {
      event.preventDefault()
      if (this.state.isVKPasswordChanged) {
        this.authorizeVK()
      }
    }
  }

  handleChangeLoginVK = event => {
    this.setState({ loginVK: event.target.value })
  }

  handleChangePasswordVK = event => {
    if (event.target.value.length === 0) {
      this.setState({ isVKPasswordChanged: false, passwordVK: undefined })
    } else {
      this.setState({ isVKPasswordChanged: true })
    }
    this.setState({ passwordVK: event.target.value })
  }

  authIcon = (<span />)

  getAuthIcon = () => {
    if (this.props.isAppSignIn === true) {
      if (this.props.isVkSignIn === true) {
        this.authIcon = (
          <MDBIcon className="icon-app" icon="battery-full" size="2x" />
        )
      } else {
        this.authIcon = (
          <MDBIcon className="icon-app" icon="battery-half" size="2x" />
        )
      }
    } else {
      this.authIcon = (
        <MDBIcon className="icon-app" icon="battery-empty" size="2x" />
      )
    }
    return this.authIcon
  }

  render() {
    return (
      <>
        <MDBBtn
          className={'top-btn col-6'}
          color="transparent"
          onClick={this.toggleApp}
        >
          {this.getAuthIcon()}
        </MDBBtn>

        <MDBModal
          className={'add-playlist-modal'}
          centered
          isOpen={this.state.showApp}
          toggle={this.toggleApp}
        >
          <MDBModalHeader toggle={this.toggleApp}>{'Профиль'}</MDBModalHeader>
          <MDBModalBody>
            <MDBContainer>
              <MDBRow>
                <MDBCol>Логин:</MDBCol>
                <MDBCol>{this.props.username}</MDBCol>
              </MDBRow>
              <MDBRow>
                <MDBCol>Статус VK:</MDBCol>
                <MDBCol>
                  {this.props.isVkSignIn ? (
                    <>
                      <span>Авторизован /</span>
                      <MDBBtn
                        className="blue-gradient"
                        size={'sm'}
                        onClick={this.toggleVK}
                      >
                        {'Сменить Аккаунт VK'}
                      </MDBBtn>
                    </>
                  ) : (
                    <MDBBtn
                      className="blue-gradient"
                      size={'sm'}
                      onClick={this.toggleVK}
                    >
                      {'Авторизоваться'}
                    </MDBBtn>
                  )}
                </MDBCol>
              </MDBRow>
            </MDBContainer>
          </MDBModalBody>
          <MDBModalFooter>
            <MDBBtn className="blue-gradient" size={'sm'} onClick={this.logout}>
              {'Выйти из приложения'}
            </MDBBtn>
          </MDBModalFooter>
        </MDBModal>

        <MDBModal centered isOpen={this.state.showVK} toggle={this.toggleVK}>
          <MDBModalHeader toggle={this.toggleVK}>
            {VK_AUTHORIZATION_HEADER}
          </MDBModalHeader>
          <MDBModalBody>
            <InputGroup className="mb-3">
              <FormControl
                id="username"
                placeholder={VK_AUTHORIZATION_LOGIN_PLACEHOLDER}
                aria-label="Username"
                value={this.state.loginVK}
                onChange={this.handleChangeLoginVK}
              />
            </InputGroup>

            <InputGroup className="mb-3">
              <FormControl
                type="password"
                id="password"
                placeholder={VK_AUTHORIZATION_PASSWORD_PLACEHOLDER}
                aria-label="password"
                onChange={this.handleChangePasswordVK}
                onKeyDown={this.handleEnterVK}
              />
            </InputGroup>
          </MDBModalBody>
          <MDBModalFooter>
            <MDBBtn className="blue-gradient" onClick={this.authorizeVK}>
              {LOGIN_BUTTON}
            </MDBBtn>
          </MDBModalFooter>
        </MDBModal>
      </>
    )
  }
}

export default withAuthProps(ProfileButton)
