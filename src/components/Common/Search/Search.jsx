import './index.css'
import React from 'react'
import ReactDOM from 'react-dom'

import axios from 'axios'
import { toggleSearchParams } from '../../../store/search/actions'
import { SEARCH_PLACEHOLDER } from '../../../constants/AppStringConstants'
import { MDBBtn, MDBCol, MDBFormInline, MDBIcon, MDBRow } from 'mdbreact'
import withMobileSearch from '../../../containers/withMobileSearchProps'
import { searchSignal, setMobileSearch } from '../../../store/mobileSearch/actions'
import { BACKEND_SOURCE } from '../../../constants/ServerConstants'
import withSearchProps from '../../../containers/withSearchProps'

class Search extends React.Component {
  constructor(props) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.onAutoComplete = this.onAutoComplete.bind(this)
    this.handleClickOutside = this.handleClickOutside.bind(this)
    this.state = {
      searchValue:
        props.match.params.q === undefined
          ? ''
          : decodeURIComponent(props.match.params.q),
      autoComplete: [],
      showAutoComplete: false,
      isSearchParams: false,
    }
    document.addEventListener('click', this.handleClickOutside, false)
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleClickOutside, false)
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (
      this.props.match.params.q !== prevProps.match.params.q &&
      this.props.match.params.q === undefined
    ) {
      this.setState({ searchValue: '' })
    }
  }

  handleClickOutside(event) {
    // Получаем элемент, на который произведен клик
    const domNode = ReactDOM.findDOMNode(this)

    // Проверяем, что элемент присутствует в переменной,
    // а так же, является ли "domNode" узел потомком "event.target" узла.
    if (!domNode || !domNode.contains(event.target)) {
      this.offAutoComplete(event)
    }
  }

  setMobileSearch = () => {
    this.props.dispatch(setMobileSearch(true))
  }

  offAutoComplete = () => {
    this.setState({ showAutoComplete: false })
  }

  handleChange = event => {
    this.setState({
      searchValue: event.target.value,
    })

    const URL = `${BACKEND_SOURCE}/autocomplete?q=${encodeURI(
      event.target.value
    )}`

    let request = axios.get(URL)

    request.then(res => {
      let autoCompleteArr = res.data.map(element => {
        return element
      })
      this.setState({ autoComplete: autoCompleteArr, showAutoComplete: true })
    })
  }

  handleSubmit = event => {
    if (event) {
      event.preventDefault()
    }
    if (this.state.searchValue && this.state.searchValue !== '') {
      // this.props.dispatch(searchMusic(this.state.searchValue,this.props.sources))
      document.body.scrollTop = document.documentElement.scrollTop = 0
      const q = encodeURIComponent(this.state.searchValue)
      this.props.history.push('/search/' + q)
    }
    this.onOverlayClick()
    this.props.dispatch(searchSignal())
  }

  onOverlayClick = () => {
    this.props.dispatch(setMobileSearch(false))
    this.setState({ showAutoComplete: false })
  }

  onAutoComplete = event => {
    const value = event.target.getAttribute('value')

    this.setState(
      {
        searchValue: value,
        showAutoComplete: false,
      },
      this.handleSubmit
    )
  }

  toggleParams = () => {
    this.props.dispatch(toggleSearchParams())
  }

  render() {
    return (
      <>
        <MDBRow>
          <MDBCol md="12">
            {this.props.mobileSearch && (
              <div
                className="mobile-search-overlay"
                onClick={this.onOverlayClick}
              />
            )}
            <MDBFormInline className="custom-search-param mr-auto" onSubmit={this.handleSubmit}>
              <MDBBtn
                gradient="blue"
                rounded
                className="custom search-btn col-2 col-md-2 ml-auto"
                onClick={this.toggleParams}
              >
                <MDBIcon icon="sliders-h" />
              </MDBBtn>
              <input
                autoComplete={"off"}
                id={'searchInput'}
                className={
                  'form-control mdb-autocomplete custom-player input  col-7 ' +
                  (!this.props.mobileSearch && 'col-md-8')
                }
                type="text"
                placeholder={SEARCH_PLACEHOLDER}
                onClick={this.setMobileSearch}
                onChange={this.handleChange}
                aria-label="Search"
                value={this.state.searchValue || ''}
              />
              <MDBBtn
                gradient="blue"
                rounded
                type="submit"
                className="custom search-btn col-3 col-md-2 ml-auto"
                onClick={this.handleSubmit}
              >
                <MDBIcon icon="search" />
              </MDBBtn>
            </MDBFormInline>
          </MDBCol>
          {this.state.showAutoComplete ? (
            <MDBCol className={'autocomplete-col col-md-12'}>
              <ul className={'autocomplete-wrap'}>
                {this.state.autoComplete.map(el => {
                  return (
                    <li key={el} value={el} onClick={this.onAutoComplete}>
                      {el}
                    </li>
                  )
                })}
              </ul>
            </MDBCol>
          ) : (
            <></>
          )}
        </MDBRow>
      </>
    )
  }
}

export default withSearchProps(withMobileSearch(Search))
