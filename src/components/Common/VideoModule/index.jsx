import s from './VideoModule.module.css'

import React, { Component } from 'react'
import ReactPlayer from 'react-player'

const sources = {
  sintelTrailer: 'http://media.w3.org/2010/05/sintel/trailer.mp4',
  bunnyTrailer: 'http://media.w3.org/2010/05/bunny/trailer.mp4',
  bunnyMovie: 'http://media.w3.org/2010/05/bunny/movie.mp4',
  test: 'http://media.w3.org/2010/05/video/movie_300.webm',
}

export default class VideoModule extends Component {
  render() {
    return (
      <div className={s.wrapper}>
        <ReactPlayer
          pip
          controls
          className={s.player}
          url="http://media.w3.org/2010/05/sintel/trailer.mp4"
          width="100%"
          height="100%"
        />
      </div>
    )
  }
}
