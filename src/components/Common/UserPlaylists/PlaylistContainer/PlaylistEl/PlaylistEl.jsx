import React from 'react'
import { NavLink } from 'react-router-dom'

const PlaylistEl = props => {
  let { playlist, thumbnail, link } = props
  const defaultThumbnail = require('../defaultThumbnail.jpg')
  if(thumbnail === undefined){
    thumbnail = defaultThumbnail
  }
  return (
    <div
      key={playlist.id}
      className={'audioPlaylists_item rowHorizontal col-4 col-sm-3 col-md-3 col-lg-20per col-xl-2'}
      title={playlist.title}
    >
      <NavLink to={link}>
        <img
          className="audioPlaylists__itemCover"
          src={thumbnail}
          onError={(e)=>{e.target.onerror = null; e.target.src=defaultThumbnail}}
          alt={'audioPlaylistItem'}/>
        <span className="audioPlaylists__itemTitle" title={playlist.title}>
          {playlist.title}
        </span>
      </NavLink>
    </div>
  )
}

export default PlaylistEl
