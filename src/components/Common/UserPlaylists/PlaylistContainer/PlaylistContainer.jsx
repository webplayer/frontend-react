import React from 'react'
import PlaylistEl from './PlaylistEl/PlaylistEl'

const PlaylistContainer = props => {
  return (
    <div className={'audioPlaylists__body row rowHorizontal no-gutters'}>
      {props.children}
      {props.playlist.map((playlist, i) => {
        let URL = '/playlist'
        if(playlist.id !== undefined){
          URL += '/id/'+ playlist.id+'/'+playlist.dynamic
        } else {
          URL += '/external/'+playlist.source+'/'+playlist.external_id+'/'+playlist.dynamic
        }

        return (
          <PlaylistEl
            playlist={playlist}
            thumbnail={playlist.thumbnail}
            link={URL}
            key={i}
          />
        )
      })}
    </div>
  )
}

export default PlaylistContainer
