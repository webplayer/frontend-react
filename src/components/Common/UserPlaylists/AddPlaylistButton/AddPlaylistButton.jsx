import React from 'react'
import { MDBBtn, MDBIcon, MDBModal, MDBModalBody, MDBModalFooter, MDBModalHeader } from 'mdbreact'
import {
  ADD_PLAYLIST_BUTTON,
  APP_CREATE_PLAYLIST_HEADER,
  PLAYLIST_NAME_PLACEHOLDER,
  PLAYLIST_THUMBNAIL_PLACEHOLDER,
} from '../../../../constants/AppStringConstants'
import InputGroup from 'react-bootstrap/InputGroup'
import FormControl from 'react-bootstrap/FormControl'
import { addPlaylist } from '../../../../store/search/actions'
import withSearchProps from '../../../../containers/withSearchProps'

class AddPlaylistButton extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      showModal: false,
      addPlaylistName: '',
      addPlaylistUrl: '',
    }
  }

  toggleModal = () => {
    this.setState({
      showModal: !this.state.showModal,
      addPlaylistName: '',
      addPlaylistUrl: '',
    })
  }


  onAddClick = () => {
    this.props.dispatch(
      addPlaylist(this.state.addPlaylistName, this.state.addPlaylistUrl)
    )
    this.toggleModal()
  }

  onNameChange = event => {
    this.setState({ addPlaylistName: event.target.value })
  }

  onUrlChange = event => {
    this.setState({ addPlaylistUrl: event.target.value })
  }

  render() {
    return (
      <>
        <MDBBtn className={this.props.btnClassName} onClick={this.toggleModal}>
          <MDBIcon icon="plus" size={'2x'} />
        </MDBBtn>
        <MDBModal
          className={'add-playlist-modal'}
          centered
          isOpen={this.state.showModal}
          toggle={this.toggleModal}
        >
          <MDBModalHeader toggle={this.toggleModal}>
            {APP_CREATE_PLAYLIST_HEADER}
          </MDBModalHeader>
          <MDBModalBody>
            <InputGroup className="mb-3">
              <FormControl
                id="playlist_name"
                placeholder={PLAYLIST_NAME_PLACEHOLDER}
                aria-label="playlist_name"
                value={this.state.addPlaylistName}
                onChange={this.onNameChange}
              />
            </InputGroup>
            <InputGroup className="mb-3">
              <FormControl
                id="playlist_thumbnail"
                placeholder={PLAYLIST_THUMBNAIL_PLACEHOLDER}
                aria-label="playlist_thumbnail"
                value={this.state.addPlaylistUrl}
                onChange={this.onUrlChange}
              />
            </InputGroup>
          </MDBModalBody>
          <MDBModalFooter>
            <MDBBtn className="blue-gradient" onClick={this.onAddClick}>
              {ADD_PLAYLIST_BUTTON}
            </MDBBtn>
          </MDBModalFooter>
        </MDBModal>
      </>
    )
  }
}

export default withSearchProps(AddPlaylistButton)
