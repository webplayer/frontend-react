import './index.css'

import React from 'react'
import { getPlaylists } from '../../../store/search/actions'
import AddPlaylistButton from './AddPlaylistButton/AddPlaylistButton'
import PlaylistContainer from './PlaylistContainer/PlaylistContainer'
import { MDBRow } from 'mdbreact'
import withSearchProps from '../../../containers/withSearchProps'

class UserPlaylists extends React.Component {
  constructor(props) {
    super(props)
    props.dispatch(getPlaylists())
  }

  render() {
    return (
      <>
        {!this.props.isPlaylistsFetching && (
          <MDBRow className={'category'} key={'userPlaylists'}>
            <div>
              <h2>{'Мои плейлисты'}</h2>
            </div>
            <PlaylistContainer playlist={this.props.userPlaylists}>
              <div key={'newPlaylist'} className={'audioPlaylists_item rowHorizontal col-4 col-4-425 col-sm-3 col-md-3 col-lg-20per col-xl-2'}>
                <AddPlaylistButton btnClassName="audioPlaylists__add blue-gradient" />
              </div>
            </PlaylistContainer>
          </MDBRow>
        )}
      </>
    )
  }
}

export default withSearchProps(UserPlaylists)
