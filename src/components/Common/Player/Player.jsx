import './index.css'

import React from 'react'
import ReactAudioPlayer from 'react-audio-player'
import withPlayerProps from '../../../containers/withPlayerProps'
import { MDBBtn, MDBBtnGroup, MDBCol, MDBContainer, MDBIcon, MDBRow } from 'mdbreact'
import * as moment from 'moment'
import { BACKEND_SOURCE } from '../../../constants/ServerConstants'
import Slider from '@material-ui/core/Slider'
import withStyles from '@material-ui/core/styles/withStyles'
import { next, pause, play, prev, setDispatch, setPlayerRef, toggleRandom } from '../../../store/player/playerControl'
import withSearchProps from '../../../containers/withSearchProps'

class Player extends React.Component {
  constructor(props) {
    super(props)
    if (localStorage.getItem('volume') === null) {
      localStorage.setItem('volume', '1')
    }

    let volume = parseFloat(localStorage.getItem('volume'))

    this.state = {
      loop: false,
      shuffle: false,
      progress: 0,
      preload: 0,
      currentTime: '00:00',
      targetTime: '00:00',
      maxTime: '00:00',
      timeLeft: '00:00',
      seek: false,
      values: [0],
      volume: volume,
      volumeValues: [
        Math.max(
          0,
          Math.min((Math.log(volume * 100 + 1) / Math.log(100)) * 100, 100)
        ),
      ],
      ended: false,
    }

    try {
      navigator.mediaSession.setActionHandler('previoustrack', prev)

      navigator.mediaSession.setActionHandler('nexttrack', next)

      navigator.mediaSession.setActionHandler('play', this.playMusic)
      navigator.mediaSession.setActionHandler('pause', this.playMusic)
    } catch (e) {}

    setDispatch(props.dispatch)
  }

  componentDidMount() {
    setPlayerRef(this.audioRef)
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.currentAudio.id !== prevProps.currentAudio.id) {
      this.setState({ progress: '0', preload: '0', maxTime: '00:00' })
    }
    // if (this.props.isPlaying === false) {
    //   let playPromise = this.audioRef.audioEl.pause()
    //   if (playPromise !== undefined) {
    //     playPromise.then(() => {}).catch(() => {})
    //   }
    // } else {
    //   let playPromise = this.audioRef.audioEl.play()
    //   if (playPromise !== undefined) {
    //     playPromise.then(() => {}).catch(() => {})
    //   }
    // }
  }

  toggleRepeat = () => {
    if (this.state.loop) {
      this.setState({ loop: false })
    } else {
      this.setState({ loop: true })
    }
  }

  togglePlayerRandom = () => {
    toggleRandom()
    if (this.state.shuffle) {
      this.setState({ shuffle: false })
    } else {
      this.setState({ shuffle: true })
    }
  }

  onEnded = () => {
    if (this.state.ended) {
      next()
      this.setState({ ended: false })
    }
  }

  changeVolume = () => {
    let audio = document.getElementById('audio')
    localStorage.setItem('volume', audio.volume)
  }

  playMusic = () => {
    if (this.props.currentAudio.length !== undefined) {
      if (this.props.isPlaying) {
        pause()
      } else {
        play()
      }
    }
  }

  onListen = event => {
    // if (
    //   !this.state.ended && !this.state.loop &&
    //   this.audioRef.audioEl.duration - this.audioRef.audioEl.currentTime <= 0.3
    // ) {
    //   this.audioRef.audioEl.pause()
    //   this.onEnded()
    //   this.setState({ ended: true })
    // }
    let buffered = this.audioRef.audioEl.buffered
    if (buffered.length) {
      let loaded = Math.round(
        (100 * buffered.end(0)) / this.audioRef.audioEl.duration
      )
      this.setState({ preload: loaded })
    }
    if (!this.state.seek) {
      let progress = (event / this.audioRef.audioEl.duration) * 100
      this.setState({ progress: progress, values: [progress] })
    }

    const currentSeconds = this.audioRef.audioEl.currentTime
    const maxSeconds = this.audioRef.audioEl.duration
    let format = 'mm:ss'
    if (maxSeconds >= 3600) {
      format = 'HH:mm:ss'
    }
    let formattedCurrent = moment.utc(currentSeconds * 1000).format(format)
    let formattedMax = moment.utc(maxSeconds * 1000).format(format)
    let formattedTimeLeft = moment
      .utc((maxSeconds - currentSeconds) * 1000)
      .format(format)

    this.setState({
      currentTime: formattedCurrent,
      maxTime: formattedMax,
      timeLeft: formattedTimeLeft,
    })
  }

  onProgressChange = (event, value) => {
    if (this.props.currentAudio !== undefined) {
      const maxSeconds = this.audioRef.audioEl.duration
      let format = 'mm:ss'
      if (maxSeconds >= 3600) {
        format = 'HH:mm:ss'
      }
      let formattedCurrent = moment
        .utc((value / 100) * maxSeconds * 1000)
        .format(format)
      let formattedTimeLeft = moment
        .utc((1 - value / 100) * maxSeconds * 1000)
        .format(format)
      this.setState({
        progress: value,
        seek: true,
        targetCurrent: formattedCurrent,
        targetTimeLeft: formattedTimeLeft,
      })
    }
  }

  onProgressChangeFinal = (event, value) => {
    if (this.props.currentAudio !== undefined) {
      this.setState({ progress: value, seek: false })
      let duration = parseFloat(this.audioRef.audioEl.duration)
      if (!Number.isNaN(duration)) {
        this.audioRef.audioEl.currentTime = (value / 100) * duration
      }
    }
  }

  onVolumeChange = (event, value) => {
    this.setState({
      volumeValues: value,
      volume: (Math.pow(100, value / 100) - 1) / 100,
    })
  }

  render() {
    const ICON_SIZE = '1x'
    let timeLeft = this.state.timeLeft
    if (timeLeft === 'Invalid date') {
      timeLeft = '00:00'
    }
    let url = BACKEND_SOURCE + this.props.currentAudio.url
    // let url = BACKEND_SOURCE

    return (
      <MDBRow className={'no-gutters'} center>
        <MDBCol className={'col-12 col-md-8 col-lg-6 col-xl-4'}>
          <ReactAudioPlayer
          id={'audio'}
          preload={'auto'}
          autoPlay
          listenInterval={1000}
          loop={this.state.loop}
          volume={this.state.volume}
          onVolumeChanged={this.changeVolume}
          onListen={this.onListen}
          onEnded={next}
          ref={element => {
            this.audioRef = element
          }}
          title={'search'}
          onPause={pause}
          onPlay={play}
          onError={next}
        />

          <div className="player_content">
            <div className="timeline_wrap">
              <div className="timeline progress-bar-wrapper">
                <div
                  className="line_preload"
                  style={{ width: this.state.preload + '%' }}
                />
                <PrettoSlider
                  step={0.01}
                  id={'musicProgressSlider'}
                  value={parseFloat(
                    isNaN(this.state.progress) ? 0.0 : this.state.progress
                  )}
                  onChange={this.onProgressChange}
                  onChangeCommitted={this.onProgressChangeFinal}
                />
              </div>
              <div className="time_of_song">
                <MDBContainer fluid>
                  <MDBRow between>
                    <MDBCol size={2} md={1}>
                      <span className="time_played">
                        {!this.state.seek
                          ? this.state.currentTime
                          : this.state.targetCurrent}
                      </span>
                    </MDBCol>
                    <MDBCol size={2} md={1}>
                      <span className="full_time">
                        {!this.state.seek
                          ? timeLeft
                          : this.state.targetTimeLeft}
                      </span>
                    </MDBCol>
                  </MDBRow>
                </MDBContainer>
              </div>
            </div>
            <div className="player_btns">
              <MDBContainer fluid>
                <MDBRow center>
                  <MDBBtnGroup>
                    <MDBBtn
                      color="transparent"
                      className="random_btn text-center"
                      id="random"
                      onClick={() => this.togglePlayerRandom()}
                    >
                      {this.state.shuffle ? (
                        <MDBIcon
                          className={'active-btn'}
                          icon="random"
                          size={ICON_SIZE}
                        />
                      ) : (
                        <MDBIcon icon="random" size={ICON_SIZE} />
                      )}
                    </MDBBtn>
                    <MDBBtn
                      color="transparent"
                      className="prev_btn text-center"
                      id="btnPrev"
                      data-action="prev"
                      onClick={() => prev()}
                    >
                      <MDBIcon icon="backward" size={ICON_SIZE} />
                    </MDBBtn>
                    <MDBBtn
                      gradient="blue"
                      circle
                      className="play_btn text-center"
                      id="play"
                      data-action="play"
                      onClick={() => this.playMusic()}
                    >
                      {this.props.isPlaying ? (
                        <MDBIcon icon="pause" size={ICON_SIZE} />
                      ) : (
                        <MDBIcon icon="play" size={ICON_SIZE} />
                      )}
                    </MDBBtn>
                    <MDBBtn
                      color="transparent"
                      className="next_btn text-center"
                      id="btnNext"
                      data-action="next"
                      onClick={() => next()}
                    >
                      <MDBIcon icon="forward" size={ICON_SIZE} />
                    </MDBBtn>
                    <MDBBtn
                      color="transparent"
                      className="repeat_btn text-center"
                      id="repeat"
                      onClick={() => this.toggleRepeat()}
                    >
                      {this.state.loop ? (
                        <MDBIcon
                          className={'active-btn'}
                          icon="retweet"
                          size={ICON_SIZE}
                        />
                      ) : (
                        <MDBIcon icon="retweet" size={ICON_SIZE} />
                      )}
                    </MDBBtn>
                  </MDBBtnGroup>
                  <div className="volume_wrap timeline_wrap">
                    <div className="timeline progress-bar-wrapper">
                      <div
                        className="line_played"
                        style={{ width: this.state.volumeValues + '%' }}
                      />
                      <PrettoSlider
                        step={0.01}
                        id={'musicProgressSlider'}
                        defaultValue={this.state.volumeValues}
                        onChange={this.onVolumeChange}
                      />
                    </div>
                  </div>
                </MDBRow>
              </MDBContainer>
            </div>
          </div>
        </MDBCol>
      </MDBRow>
    )
  }
}

const PrettoSlider = withStyles({
  root: {
    color: 'rgb(255,255,255)',
    height: 4,
  },
  thumb: {
    height: 10,
    width: 10,
    backgroundColor: '#fff',
    border: '2px solid currentColor',
    '&:focus,&:hover,&$active': {
      boxShadow: 'inherit',
    },
  },
  active: {},
  valueLabel: {
    left: 'calc(-50% + 4px)',
  },
  track: {
    height: 2,
    borderRadius: 4,
  },
  rail: {
    height: 2,
    borderRadius: 4,
  },
})(Slider)

export default withSearchProps(withPlayerProps(Player))
