import './index.css'

import React from 'react'
import { Field, Form, Formik } from 'formik'
import { MDBBtn, MDBContainer, MDBRow } from 'mdbreact'
import withAuthProps from '../../../containers/withAuthProps'
import { signUp } from '../../../store/auth/actions'

class SignUpForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  validateLogin(value) {
    let error
    if (!value) {
      error = 'Логин не может быть пустым'
    } else if (value.length < 3) {
      error = 'Логин слишком короткий < 3'
    } else if (!/^[0-9a-zA-Z]+$/i.test(value)) {
      error = 'Логин должен состоять из латинских символов и цифр от 0 до 9'
    } else if (value.length > 255) {
      error = 'Логин слишком длинный > 255'
    }
    return error
  }

  validatePassword(value) {
    let error
    if (!value) {
      error = 'Пароль не может быть пустым'
    } else if (value.length < 8) {
      error = 'Пароль слишком короткий < 8'
    } else if (!/^[0-9a-zA-Z]+$/i.test(value)) {
      error = 'Пароль должен состоять из латинских символов и цифр от 0 до 9'
    } else if (value.length > 255) {
      error = 'Пароль слишком длинный > 255'
    }
    return error
  }

  validatePasswords(values) {
    const errors = {}
    if (values.password) {
      if (values.passwordRepeat) {
        if (values.password !== values.passwordRepeat) {
          errors.passwordRepeat = 'Пароли на совпадают'
        }
      }
    }
    return errors
  }

  render() {
    return (
      <Formik
        initialValues={{
          login: '',
          password: '',
          passwordRepeat: '',
        }}
        onSubmit={values => {
          // same shape as initial values
          console.log(values)
          this.props.dispatch(signUp(values.login, values.password))
        }}
        validate={this.validatePasswords}
      >
        {({ errors, touched}) => (
          <Form>
            <label htmlFor="defaultFormRegisterNameEx" className="grey-text">
              Логин
            </label>
            <Field
              className={
                errors.login && touched.login
                  ? 'form-control mb-3 form-invalid'
                  : 'form-control mb-3'
              }
              name="login"
              validate={this.validateLogin}
            />
            {errors.login && touched.login && (
              <div className={'feedback-invalid'}>{errors.login}</div>
            )}

            <label htmlFor="defaultFormRegisterNameEx" className="grey-text">
              Пароль
            </label>
            <Field
              className={
                errors.password && touched.password
                  ? 'form-control mb-3 form-invalid'
                  : 'form-control mb-3'
              }
              name="password"
              type="password"
              validate={this.validatePassword}
            />
            {errors.password && touched.password && (
              <div className={'feedback-invalid'}>{errors.password}</div>
            )}

            <label htmlFor="defaultFormRegisterNameEx" className="grey-text">
              Повтор пароля
            </label>
            <Field
              className={
                errors.passwordRepeat && touched.passwordRepeat
                  ? 'form-control mb-3 form-invalid'
                  : 'form-control mb-3'
              }
              name="passwordRepeat"
              type="password"
            />
            {errors.passwordRepeat && touched.passwordRepeat && (
              <div className={'feedback-invalid'}>{errors.passwordRepeat}</div>
            )}

            <MDBContainer fluid>
              <MDBRow fluid end>
                <MDBBtn className="blue-gradient" size={'sm'} type="submit">
                  {'Зарегистрироваться'}
                </MDBBtn>
              </MDBRow>
            </MDBContainer>

          </Form>
        )}
      </Formik>
    )
  }
}

export default withAuthProps(SignUpForm)
