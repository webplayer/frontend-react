import React from 'react'
import './style.css'

const DownloadSpinner = () => {
  return (
    <video className={'img-fluid'} autoPlay loop muted playsInline>
      <source src={require('./wait.webm')} type="video/webm" />
      <source src={require('./wait.mp4')} type="video/mp4" />
    </video>
  )
}

export default DownloadSpinner
