import './index.css'
import React from 'react'
import withAuthProps from '../../../containers/withAuthProps'
import { appSignIn } from '../../../store/auth/actions'
import InputGroup from 'react-bootstrap/InputGroup'
import FormControl from 'react-bootstrap/FormControl'
import { APP_AUTHORIZATION_HEADER, LOGIN_BUTTON } from '../../../constants/AuthorizationConstants'
import {
  MDBBtn,
  MDBContainer,
  MDBIcon,
  MDBModal,
  MDBModalBody,
  MDBModalFooter,
  MDBModalHeader,
  MDBRow,
} from 'mdbreact'
import SignUpForm from '../SignUpForm/SignUpForm'
import withErrorMessages from '../../../containers/withErrorMessages'

class AuthButton extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      showApp: !props.isAppSignIn,
      showVK: false,
      loginVK: '',
      passwordVK: '',
      loginApp: '',
      passwordApp: '',
      isVKPasswordChanged: false,
      isAppPasswordChanged: false,
      login: '',
      password: '',
      repeatPassword: '',
      isSubmit: false,
    }
  }

  toggleApp = () => {
    if (this.props.isAppSignIn) {
      this.setState({ showApp: !this.state.showApp })
    }
  }

  toggleVK = () => {
    this.setState({ showVK: !this.state.showVK, showApp: !this.state.showApp })
  }

  authorizeApp = () => {
    if (this.state.loginApp !== '' && this.state.passwordApp !== '') {
      this.props.dispatch(
        appSignIn(this.state.loginApp, this.state.passwordApp)
      )
    }
    this.setState({ isSubmit: true })
  }

  handleEnterApp = event => {
    if (event.key === 'Enter') {
      event.preventDefault()
      if (this.state.isAppPasswordChanged) {
        this.authorizeApp()
      }
    }
  }

  handleChangeLoginApp = event => {
    this.setState({ loginApp: event.target.value })
  }

  handleChangePasswordApp = event => {
    if (event.target.value.length === 0) {
      this.setState({ isAppPasswordChanged: false, passwordApp: undefined })
    } else {
      this.setState({ isAppPasswordChanged: true })
    }
    this.setState({ passwordApp: event.target.value })
  }

  render() {
    return (
      <>
        <MDBBtn
          className={'top-btn col-6'}
          color="transparent"
          onClick={this.toggleApp}
        >
          <MDBIcon className="icon-app" icon="battery-empty" size="2x" />
        </MDBBtn>

        <MDBModal size={'sm'} centered isOpen={this.state.showApp} toggle={this.toggleApp}>
          <MDBModalHeader>{APP_AUTHORIZATION_HEADER}</MDBModalHeader>
          <MDBModalBody>
            <label htmlFor="defaultFormRegisterNameEx" className="grey-text">
              Логин
            </label>
            <InputGroup className="mb-3">
              <FormControl
                id="username"
                aria-label="Username"
                value={this.state.loginApp}
                onChange={this.handleChangeLoginApp}
              />
            </InputGroup>

            <label htmlFor="defaultFormRegisterNameEx" className="grey-text">
              Пароль
            </label>
            <InputGroup className="mb-3">
              <FormControl
                type="password"
                id="password"
                aria-label="password"
                onChange={this.handleChangePasswordApp}
                onKeyDown={this.handleEnterApp}
              />
            </InputGroup>

            {!this.props.isAppSignInFetching &&
              !this.props.isAppSignIn &&
              this.state.isSubmit && (
                <div className={'feedback-invalid'}>
                  {this.props.errorResponse === 'Unauthorized' &&
                    'Неверный логин или пароль'}
                </div>
              )}
          </MDBModalBody>
          <MDBModalFooter>
            <MDBContainer fluid>
              <MDBRow between>
                <MDBBtn className="blue-gradient" size={'sm'} onClick={this.toggleVK}>
                  {'Регистрация'}
                </MDBBtn>
                <MDBBtn className="blue-gradient" size={'sm'} onClick={this.authorizeApp}>
                  {LOGIN_BUTTON}
                </MDBBtn>
              </MDBRow>
            </MDBContainer>
          </MDBModalFooter>
        </MDBModal>

        <MDBModal centered isOpen={this.state.showVK} toggle={this.toggleVK}>
          <MDBModalHeader toggle={this.toggleVK}>
            {'Регистрация'}
          </MDBModalHeader>
          <MDBModalBody>
            <SignUpForm />
          </MDBModalBody>
        </MDBModal>
      </>
    )
  }
}

export default withAuthProps(withErrorMessages(AuthButton))
