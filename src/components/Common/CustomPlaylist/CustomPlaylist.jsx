import './index.css'

import React from 'react'
import withPlayerProps from '../../../containers/withPlayerProps'
import { deleteAudio } from '../../../store/search/actions'
import { MDBBtn, MDBIcon, MDBModal, MDBModalBody, MDBModalFooter, MDBModalHeader, MDBRow } from 'mdbreact'
import * as moment from 'moment'
import s from '../../AppStructure/Pages/PopularPage/PopularPage.module.css'
import PlaylistContainer from '../UserPlaylists/PlaylistContainer/PlaylistContainer'
import withSearchProps from '../../../containers/withSearchProps'
import { pause, play, setPlaylist } from '../../../store/player/playerControl'

class CustomPlaylist extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      showModalDelete: false,
      radio: undefined,
      deleteAudio: undefined,
      shadowPlaylist: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    }
  }

  toggleModalDelete = () => {
    this.setState({
      showModalDelete: !this.state.showModalDelete,
    })
  }

  onDeleteConfirmClick = () => {
    this.props.dispatch(
      deleteAudio(this.props.currentPlaylistId, this.state.deleteAudio)
    )
    this.toggleModalDelete()
  }

  onSelect = event => {
    this.setState({
      radio: event.currentTarget.value,
    })
  }

  onPlay = music => {
    if (this.props.currentAudio.id === music.id) {
      if (this.props.isPlaying === true) {
        pause()
      } else {
        play()
      }
    } else {
      setPlaylist(this.props.tempPlaylist, music)
    }
  }

  onDeleteClick = event => {
    this.toggleModalDelete()
    this.setState({ deleteAudio: event.currentTarget.value })
  }

  render() {
    let { isFetching, tempPlaylist } = this.props
    return (
      <>
        {isFetching ? (
          <>
            {this.state.shadowPlaylist.map((el, idx) => (
              <MDBRow key={idx} className={'musicEl'}>
                <div className={'thumbnailWrapper'}>
                  <div
                    className={'MusicThumbnail'}
                    style={{ backgroundImage: 'url(' + ')' }}
                  />
                </div>
              </MDBRow>
            ))}
          </>
        ) : (
          <>
            {tempPlaylist.map(music => {
              if (music.list) {
                let formattedList = music.list.data.map(el => {
                  return el.playlist
                })
                return (
                  <MDBRow className={s.category} key={music.list.title}>
                    <div>
                      <h2>{music.list.title}</h2>
                    </div>
                    <PlaylistContainer playlist={formattedList} />
                  </MDBRow>
                )
              }
              if (music.audio) {
                music = music.audio
                let thumbnail = music.thumbnail
                if (music.thumbnail === null) {
                  thumbnail = require('../Playlist/AudioEl/thumbnail.png')
                }

                let isCurrent = false
                if (music.id === this.props.currentAudio.id) {
                  isCurrent = true
                }

                let isPlaying = this.props.isPlaying

                let id = music.id

                let format = 'mm:ss'
                if (music.length >= 3600) {
                  format = 'HH:mm:ss'
                }
                let length = moment.utc(music.length * 1000).format(format)

                return (
                  <MDBRow
                    className={isCurrent ? 'musicEl activeMusic' : 'musicEl'}
                    key={id}
                  >
                    <div className={'thumbnailWrapper'}>
                      <div
                        className={'MusicThumbnail'}
                        style={{ backgroundImage: 'url(' + thumbnail + ')' }}
                        onClick={event => {
                          event.preventDefault()
                          this.onPlay(music)
                        }}
                      />
                    </div>
                    <div
                      className={isCurrent ? 'musicInfoActive' : 'musicInfo'}
                      onClick={event => {
                        event.preventDefault()
                        this.onPlay(music)
                      }}
                    >
                      <span
                        className={
                          isCurrent
                            ? 'text musicTitle activeMusicEl'
                            : 'text musicTitle'
                        }
                        title={music.title}
                      >
                        {music.title}
                      </span>
                      <span className={'text musicArtist'}>{music.artist}</span>

                      <span className={'musicTime'}>{length}</span>
                    </div>

                    {!this.props.immutable && (
                      <MDBBtn
                        value={id}
                        className="menu_wrap"
                        size={'sm'}
                        color="transparent"
                        onClick={this.onDeleteClick}
                      >
                        <MDBIcon icon="ellipsis-v" size={'lg'} />
                      </MDBBtn>
                    )}
                  </MDBRow>
                )
              }
              return <></>
            })}

            <MDBModal
              className={'add-playlist-modal'}
              centered
              isOpen={this.state.showModalDelete}
              toggle={this.toggleModalDelete}
            >
              <MDBModalHeader toggle={this.toggleModalDelete}>
                {'Удаление аудио'}
              </MDBModalHeader>
              <MDBModalBody>
                Вы действительно хотите удалить аудио?
              </MDBModalBody>
              <MDBModalFooter>
                <MDBBtn
                  size={'sm'}
                  className="blue-gradient"
                  onClick={this.toggleModalDelete}
                >
                  {'Отмена'}
                </MDBBtn>
                <MDBBtn
                  size={'sm'}
                  className="blue-gradient"
                  onClick={this.onDeleteConfirmClick}
                >
                  {'Да, удалить'}
                </MDBBtn>
              </MDBModalFooter>
            </MDBModal>
          </>
        )}
      </>
    )
  }
}

export default withPlayerProps(withSearchProps(CustomPlaylist))
