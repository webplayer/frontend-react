import React from 'react'
import { MDBBtn, MDBIcon, MDBModal, MDBModalBody, MDBModalFooter, MDBModalHeader, MDBRow } from 'mdbreact'
import { PLAYLIST_NAME_PLACEHOLDER, PLAYLIST_THUMBNAIL_PLACEHOLDER } from '../../../constants/AppStringConstants'
import InputGroup from 'react-bootstrap/InputGroup'
import FormControl from 'react-bootstrap/FormControl'
import { deletePlaylist, editPlaylist } from '../../../store/search/actions'
import withSearchProps from '../../../containers/withSearchProps'
import s from '../Playlist/PlaylistEl/PlaylistEl.module.css'
import './index.css'

class EditPlaylistModal extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      showModal: props.show,
      showModalDelete: false,
      playlistName: '',
      playlistUrl: '',
    }
  }

  toggleModal = () => {
    this.setState({
      showModal: !this.state.showModal,
      playlistName: '',
      playlistUrl: '',
    })
  }

  toggleModalDelete = () => {
    this.setState({
      showModal: !this.state.showModal,
      showModalDelete: !this.state.showModalDelete,
    })
  }

  onEditClick = () => {
    this.props.dispatch(
      editPlaylist(
        this.props.currentPlaylistId,
        this.state.playlistName,
        this.state.playlistUrl
      )
    )
    this.toggleModal()
  }

  onDeleteClick = () => {
    this.props.dispatch(deletePlaylist(this.props.currentPlaylistId))
    this.props.history.push('/')
  }

  onNameChange = event => {
    this.setState({ playlistName: event.target.value })
  }

  onUrlChange = event => {
    this.setState({ playlistUrl: event.target.value })
  }

  render() {
    let playlist = this.props.userPlaylists.find(
      el => el.id == this.props.currentPlaylistId
    )
    let isReady = playlist !== undefined

    let isDefault = false
    if (isReady) {
      isDefault = playlist.thumbnail == ''
    }

    if (this.props.currentPlaylistId === undefined) {
      isReady = false
    }

    return (
      <>
        {isReady && (
          <MDBRow className={s.playlistEl}>
            <div className="PlaylistThumbnailWrapper">
              {!isDefault ? (
                <div
                  style={{
                    backgroundImage: 'url(' + playlist.thumbnail + ')',
                  }}
                  className={'PlaylistThumbnail'}
                />
              ) : (
                <div
                  style={{
                    backgroundImage:
                      'url(' +
                      require('../UserPlaylists/PlaylistContainer/defaultThumbnail.jpg') +
                      ')',
                  }}
                  className={'PlaylistThumbnail'}
                />
              )}
            </div>
            <div className={s.playlistInfo}>
              <div className={'PlaylistTitle'} title={playlist.title}>
                {playlist.title}
              </div>
              <div className={'text-truncate'}>
                {playlist.size !== undefined && playlist.size + ' Аудио'}
              </div>
            </div>
            <MDBBtn
              color="transparent"
              size={this.props.btnSize}
              className={'editPlaylistButton'}
              onClick={this.toggleModal}
            >
              <MDBIcon icon="ellipsis-v" size={'lg'} />
            </MDBBtn>
          </MDBRow>
        )}
        <MDBModal
          className={'add-playlist-modal'}
          centered
          isOpen={this.state.showModal}
          toggle={this.toggleModal}
        >
          <MDBModalHeader toggle={this.toggleModal}>
            {'Редактировать плейлист'}
          </MDBModalHeader>
          <MDBModalBody>
            <InputGroup className="mb-3">
              <FormControl
                id="playlist_name"
                placeholder={PLAYLIST_NAME_PLACEHOLDER}
                aria-label="playlist_name"
                value={this.state.playlistName}
                onChange={this.onNameChange}
              />
            </InputGroup>
            <InputGroup className="mb-3">
              <FormControl
                id="playlist_thumbnail"
                placeholder={PLAYLIST_THUMBNAIL_PLACEHOLDER}
                aria-label="playlist_thumbnail"
                value={this.state.playlistUrl}
                onChange={this.onUrlChange}
              />
            </InputGroup>
          </MDBModalBody>
          <MDBModalFooter className={'editModalFooter'}>
            <MDBBtn size={'sm'} color="danger" onClick={this.toggleModalDelete}>
              {'Удалить'}
            </MDBBtn>
            <MDBBtn
              size={'sm'}
              className="blue-gradient"
              onClick={this.onEditClick}
            >
              {'Сохранить'}
            </MDBBtn>
          </MDBModalFooter>
        </MDBModal>

        <MDBModal
          className={'add-playlist-modal'}
          centered
          isOpen={this.state.showModalDelete}
          toggle={this.toggleModalDelete}
        >
          <MDBModalHeader toggle={this.toggleModalDelete}>
            {'Удаление плейлиста'}
          </MDBModalHeader>
          <MDBModalBody>Вы действительно хотите удалить плейлист?</MDBModalBody>
          <MDBModalFooter>
            <MDBBtn
              size={'sm'}
              className="blue-gradient"
              onClick={this.toggleModalDelete}
            >
              {'Отмена'}
            </MDBBtn>
            <MDBBtn
              size={'sm'}
              className="blue-gradient"
              onClick={this.onDeleteClick}
            >
              {'Да, удалить'}
            </MDBBtn>
          </MDBModalFooter>
        </MDBModal>
      </>
    )
  }
}

export default withSearchProps(EditPlaylistModal)
