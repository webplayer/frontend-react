import './index.css'
import React from 'react'
import withAuthProps from '../../../containers/withAuthProps'
import { MDBBtn, MDBIcon, MDBModal, MDBModalBody, MDBModalFooter, MDBModalHeader } from 'mdbreact'
import InputGroup from 'react-bootstrap/InputGroup'
import FormControl from 'react-bootstrap/FormControl'
import { APP_SETTINGS_HEADER } from '../../../constants/AppStringConstants'
import { setBackgroundImage } from '../../../store/auth/actions'

class AppSettings extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      modal: false,
      backgroundImage: '',
      placeholder: this.props.backgroundImage,
    }
  }

  toggle = () => {
    this.setState({
      modal: !this.state.modal,
    })
  }

  handleChangeUrl = event => {
    this.setState({ backgroundImage: event.target.value })
  }

  saveBackgroundUrl = () => {
    this.props.dispatch(setBackgroundImage(this.state.backgroundImage))
  }

  resetBackgroundUrl = () => {
    const resetUrl = require('../../App/back_3840x2400.webp')
    this.props.dispatch(setBackgroundImage(resetUrl))
    this.setState({ backgroundImage: '' })
  }

  render() {
    return (
      <>
        <MDBBtn
          className="settings-btn top-btn col-6"
          color="transparent"
          onClick={this.toggle}
        >
          <MDBIcon className="icon-settings" fab size={'2x'} icon="whmcs" />
        </MDBBtn>
        <MDBModal
          centered
          size={'sm'}
          isOpen={this.state.modal}
          toggle={this.toggle}
        >
          <MDBModalHeader toggle={this.toggle}>
            {APP_SETTINGS_HEADER}
          </MDBModalHeader>
          <MDBModalBody>
            <InputGroup className="mb-3">
              <FormControl
                id="backgroundUrl"
                placeholder={this.state.placeholder}
                value={this.state.backgroundImage}
                aria-label="backgroundUrl"
                onChange={this.handleChangeUrl}
              />
            </InputGroup>
          </MDBModalBody>
          <MDBModalFooter>
            <MDBBtn
              size="sm"
              className="blue-gradient"
              onClick={this.resetBackgroundUrl}
            >
              {'Сбросить'}
            </MDBBtn>
            <MDBBtn
              size="sm"
              className="blue-gradient"
              onClick={this.saveBackgroundUrl}
            >
              {'Сохранить'}
            </MDBBtn>
          </MDBModalFooter>
        </MDBModal>
      </>
    )
  }
}

export default withAuthProps(AppSettings)
