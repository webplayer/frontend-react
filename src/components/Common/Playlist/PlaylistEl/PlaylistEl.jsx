import s from './PlaylistEl.module.css'

import React from 'react'
import { MDBBtn, MDBIcon, MDBRow } from 'mdbreact'
import { NavLink } from 'react-router-dom'
import withSearchProps from '../../../../containers/withSearchProps'

const PlaylistEl = props => {
  let { playlist, isChannel, link, dispatch, onAddClick } = props

  // const onAddClick = (event) => {
  //   event.preventDefault()
  //   dispatch(addExternalPlaylist(playlist.external_id,playlist.source,playlist.title))
  // }

  return (
    <MDBRow key={playlist.external_id} className={s.playlistEl}>
      <div className="PlaylistThumbnailWrapper">
        <NavLink to={link}>
          <div
            style={{
              backgroundImage: 'url(' + playlist.thumbnail + ')',
            }}
            className={isChannel ? 'ChannelThumbnail' : 'PlaylistThumbnail'}
          />
        </NavLink>
      </div>
      <div className={isChannel ? s.channelInfo : s.playlistInfo}>
        <NavLink className={s.link} to={link}>
          <div className={'PlaylistTitle'} title={playlist.title}>
            {playlist.title}
          </div>
          <div className={'text-truncate'}>
            {!isChannel
              ? playlist.size !== undefined && playlist.size + ' Аудио'
              : '999.9 подписчиков'}
          </div>
        </NavLink>
      </div>
      <MDBBtn
        value={
          playlist.external_id +
          '#$%#' +
          playlist.source +
          '#$%#' +
          playlist.title +
          '#$%#' +
          playlist.thumbnail
        }
        className={s.addButton}
        size={'sm'}
        color="transparent"
        onClick={onAddClick}
      >
        <MDBIcon icon="ellipsis-v" size={'lg'} />
      </MDBBtn>
    </MDBRow>
  )
}

export default withSearchProps(PlaylistEl)
