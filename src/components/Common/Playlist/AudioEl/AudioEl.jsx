import './index.css'

import React from 'react'
import { MDBBtn, MDBIcon, MDBRow } from 'mdbreact'
import * as moment from 'moment'

const AudioEl = props => {
  let { music, isCurrent, isPlaying, onPlay, onAddClick } = props

  let thumbnail = music.thumbnail
  if (music.thumbnail === null) {
    thumbnail = require('./thumbnail.png')
  }

  let id = music.source + '_' + music.id

  let format = 'mm:ss'
  if (music.length >= 3600) {
    format = 'HH:mm:ss'
  }
  let length = moment.utc(music.length * 1000).format(format)
  if(length === "Invalid date"){
    length=""
  }

  return (
    <MDBRow className={ isCurrent ? 'musicEl activeMusic' :  'musicEl'} key={id}>
      <div
        className={
         'thumbnailWrapper'
        }
      >
        <div
          className={'MusicThumbnail'}
          style={{ backgroundImage: 'url(' + thumbnail + ')' }}
          onClick={event => {
            event.preventDefault()
            onPlay(music)
          }}
        />
      </div>
      <div
        className={isCurrent ? 'musicInfoActive' : 'musicInfo'}
        onClick={event => {
          event.preventDefault()
          onPlay(music)
        }}
      >
        <span
          className={
            isCurrent ? 'text musicTitle activeMusicEl' : 'text musicTitle'
          }
          title={music.title}
        >
          {music.title}
        </span>
        <span
          className={
            'text musicArtist'
          }
        >
          {music.artist}
        </span>
        {<span className={'musicTime'}>
          {length}
        </span>}
      </div>
      <MDBBtn
        value={id}
        className="menu_wrap"
        size={'sm'}
        color="transparent"
        onClick={onAddClick}
      >
        <MDBIcon icon="ellipsis-v" size={'lg'} />
      </MDBBtn>
    </MDBRow>
  )
}

export default AudioEl
