import './index.css'

import React from 'react'
import withPlayerProps from '../../../containers/withPlayerProps'
import { addAudio, addExternalPlaylist } from '../../../store/search/actions'
import { MDBBtn, MDBCol, MDBModal, MDBModalBody, MDBModalFooter, MDBModalHeader } from 'mdbreact'
import { ADD_PLAYLIST_BUTTON, APP_SELECT_PLAYLIST_HEADER } from '../../../constants/AppStringConstants'
import AudioEl from './AudioEl/AudioEl'
import PlaylistEl from './PlaylistEl/PlaylistEl'
import withSearchProps from '../../../containers/withSearchProps'
import { pause, play, setPlaylist } from '../../../store/player/playerControl'

class Playlist extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      showModalAudio: false,
      showModalPlaylist: false,
      radio: undefined,
      addAudio: undefined,
      shadowPlaylist: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    }
  }

  onAudioSave = () => {
    this.props.dispatch(addAudio(this.state.radio, this.state.addAudio))
    this.toggleModalAudio()
  }

  onPlaylistSave = () => {
    let params = this.state.addPlaylist.split("#$%#")
    console.log(params)
    this.props.dispatch(
      addExternalPlaylist(
        params[0],
        params[1],
        params[2],
        params[3]
      )
    )
    this.toggleModalPlaylist()
  }

  onSelect = event => {
    this.setState({
      radio: event.currentTarget.value,
    })
  }

  onPlay = music => {
    if (this.props.currentAudio.id === music.id) {
      if (this.props.isPlaying === true) {
        pause()
      } else {
        play()
      }
    } else {
      setPlaylist(this.props.tempPlaylist, music)
    }
  }

  toggleModalAudio = () => {
    this.setState({ showModalAudio: !this.state.showModalAudio })
  }
  toggleModalPlaylist = () => {
    this.setState({ showModalPlaylist: !this.state.showModalPlaylist })
  }

  onAddAudioClick = event => {
    this.toggleModalAudio()
    this.setState({ addAudio: event.currentTarget.value })
  }
  onAddPlaylistClick = event => {
    this.toggleModalPlaylist()
    this.setState({ addPlaylist: event.currentTarget.value })
  }

  render() {
    let { isFetching, tempPlaylist } = this.props
    return (
      <>
        {isFetching ? (
          <>
            {this.state.shadowPlaylist.map((el, idx) => (
              <>{(Math.random() > 0.5) ? (<AudioEl
                key={idx}
                music={{thumbnail :  ""}}
                onPlay={this.onPlay}
                isCurrent={false}
                isPlaying={false}
                onAddClick={null}
              />) : (<PlaylistEl
                key={idx}
                isChannel={false}
                playlist={{thumbnail :  ""}}
                link={""}
                onAddClick={null}
              />)}</>
            ))}
          </>
        ) : (
          <>
            {tempPlaylist.map(element => {
              if (element.audio) {
                let music = element.audio

                let isCurrent = false
                if (music.id === this.props.currentAudio.id) {
                  isCurrent = true
                }

                let isPlaying = this.props.isPlaying

                return (
                  <AudioEl
                    key={music.id}
                    music={music}
                    onPlay={this.onPlay}
                    isCurrent={isCurrent}
                    isPlaying={isPlaying}
                    onAddClick={this.onAddAudioClick}
                  />
                )
              } else if (element.playlist) {
                let playlist = element.playlist
                let url =
                  '/playlist/external/' +
                  element.playlist.source +
                  '/' +
                  element.playlist.external_id +
                  '/true'
                let isChannel = element.playlist.external_id + ''
                isChannel = isChannel.startsWith('UC')

                return (
                  <PlaylistEl
                    key={playlist.external_id}
                    isChannel={isChannel}
                    playlist={playlist}
                    link={url}
                    onAddClick={this.onAddPlaylistClick}
                  />
                )
              }
              return <></>
            })}

            <MDBModal
              className={'select-playlist-modal'}
              centered
              isOpen={this.state.showModalAudio}
              toggle={this.toggleModalAudio}
            >
              <MDBModalHeader toggle={this.toggleModalAudio}>
                {APP_SELECT_PLAYLIST_HEADER}
              </MDBModalHeader>
              <MDBModalBody>
                {this.props.userPlaylists
                  .filter(el => {
                    return !el.dynamic
                  })
                  .map(el => {
                    let radioState = this.state.radio
                    let elState = el.id
                    let isActive = radioState == elState
                    return (
                      <MDBCol key={el.id} size={12}>
                        <MDBBtn
                          color={isActive ? 'blue-grey' : 'transparent'}
                          value={el.id}
                          className={'col-12'}
                          onClick={this.onSelect}
                        >
                          {el.title}
                        </MDBBtn>
                      </MDBCol>
                    )
                  })}
              </MDBModalBody>
              <MDBModalFooter>
                <MDBBtn className="blue-gradient" onClick={this.onAudioSave}>
                  {ADD_PLAYLIST_BUTTON}
                </MDBBtn>
              </MDBModalFooter>
            </MDBModal>
            <MDBModal
              className={'select-playlist-modal'}
              centered
              isOpen={this.state.showModalPlaylist}
              toggle={this.toggleModalPlaylist}
            >
              <MDBModalHeader toggle={this.toggleModalPlaylist}>
                {"Сохранить плейлист?"}
              </MDBModalHeader>
              <MDBModalFooter>
                <MDBBtn
                  color="blue-grey"
                  onClick={this.toggleModalPlaylist}
                >
                  {'Отменить'}
                </MDBBtn>
                <MDBBtn className="blue-gradient" onClick={this.onPlaylistSave}>
                  {"Сохранить"}
                </MDBBtn>
              </MDBModalFooter>
            </MDBModal>
          </>
        )}
      </>
    )
  }
}

export default withPlayerProps(withSearchProps(Playlist))
