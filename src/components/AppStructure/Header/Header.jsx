import React from 'react'
import { Route } from 'react-router-dom'
import Navigation from '../../Common/Navigation/Navigation'

const Header = () => {
  // let style = props.style
  return <>
    <Route path={'/'} component={props => <Navigation {...props} />} />
    <Route path={'/search/:q'} component={props => <Navigation {...props} />} />
    </>
}

export default Header
