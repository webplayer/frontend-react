import React from 'react'
import { getExternalAudio, searchMusicNext } from '../../../../store/search/actions'
import Playlist from '../../../Common/Playlist/Playlist'
import withSearchProps from '../../../../containers/withSearchProps'

class YoutubeChannelPage extends React.Component {
  constructor(props) {
    super(props)
    if (!props.isPlaylistsFetching) {
      const id = this.props.match.params.id
      if (id) {
        this.props.dispatch(getExternalAudio('youtube/channel', id))
      }
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (
      prevProps.match.params.id !== this.props.match.params.id ||
      (prevProps.isPlaylistsFetching === true &&
        this.props.isPlaylistsFetching === false) ||
      prevProps.match.path !== this.props.match.path
    ) {
      const id = this.props.match.params.id
      if (id) {
        this.props.dispatch(getExternalAudio('youtube/channel', id))
      }
    }
  }

  componentDidMount() {
    window.addEventListener('scroll', this.onScroll)
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.onScroll)
  }

  onScroll = () => {
    let scrollHeight = Math.max(
      document.body.scrollHeight,
      document.documentElement.scrollHeight,
      document.body.offsetHeight,
      document.documentElement.offsetHeight,
      document.body.clientHeight,
      document.documentElement.clientHeight
    )

    // Preload music
    const spacePreload = 0

    const toBottom =
      scrollHeight - window.scrollY - 1 - window.innerHeight - spacePreload

    const isNextLoad = toBottom <= 0

    if (
      isNextLoad &&
      !this.props.isFetchingNext &&
      !this.props.isFetching &&
      this.props.nextSearchUrl !== null
    ) {
      this.props.dispatch(
        searchMusicNext(this.props.currentSearch, this.props.nextSearchUrl)
      )
    }
  }

  render() {
    return <Playlist />
  }
}

export default withSearchProps(YoutubeChannelPage)
