import React from 'react'
import Playlist from '../../../Common/Playlist/Playlist'
import { searchMusic, searchMusicNext } from '../../../../store/search/actions'
import { DEFAULT_SEARCH } from '../../../../constants/AppStringConstants'
import withSearchProps from '../../../../containers/withSearchProps'
import withMobileSearch from '../../../../containers/withMobileSearchProps'

class SearchPage extends React.Component {
  constructor(props) {
    super(props)
    const q = decodeURIComponent(this.props.match.params.q)
    if (this.props.sources !== undefined) {
      if (q) {
        this.props.dispatch(
          searchMusic(q, this.props.sources, this.props.dispatch)
        )
      } else {
        this.props.dispatch(
          searchMusic(DEFAULT_SEARCH, this.props.sources, this.props.dispatch)
        )
      }
    }
  }

  componentDidMount() {
    window.addEventListener('scroll', this.onScroll)
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.onScroll)
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (
      prevProps.match.params.q !== this.props.match.params.q ||
      prevProps.sources !== this.props.sources ||
      prevProps.signal !== this.props.signal
    ) {
      const q = decodeURIComponent(this.props.match.params.q)
      if (q) {
        this.props.dispatch(
          searchMusic(q, this.props.sources, this.props.dispatch)
        )
      } else {
        this.props.dispatch(
          searchMusic(DEFAULT_SEARCH, this.props.sources, this.props.dispatch)
        )
      }
    }
  }

  onScroll = () => {
    let scrollHeight = Math.max(
      document.body.scrollHeight,
      document.documentElement.scrollHeight,
      document.body.offsetHeight,
      document.documentElement.offsetHeight,
      document.body.clientHeight,
      document.documentElement.clientHeight
    )

    // Preload music
    const spacePreload = 300

    const toBottom =
      scrollHeight - window.scrollY - 1 - window.innerHeight - spacePreload

    const isNextLoad = toBottom <= 0

    if (
      isNextLoad &&
      !this.props.isFetchingNext &&
      !this.props.isFetching &&
      this.props.nextSearchUrl !== null
    ) {
      this.props.dispatch(searchMusicNext(this.props.sources))
    }
  }

  render() {
    return <Playlist />
  }
}

export default withSearchProps(withMobileSearch(SearchPage))
