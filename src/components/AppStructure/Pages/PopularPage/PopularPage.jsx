import s from './PopularPage.module.css'

import React from 'react'
import { MDBRow } from 'mdbreact'
import DownloadSpinner from '../../../Common/DownloadSpinner/DownloadSpinner'
import { getPopularPage } from '../../../../store/search/actions'
import PlaylistContainer from '../../../Common/UserPlaylists/PlaylistContainer/PlaylistContainer'
import withSearchProps from '../../../../containers/withSearchProps'

class PopularPage extends React.Component {
  constructor(props) {
    super(props)
    if (this.props.popularPage.length === 0) this.props.dispatch(getPopularPage())
  }

  render() {
    return (
      <>
        {this.props.isFetching ? (
          <MDBRow className="downloadSpinner justify-content-center">
            <DownloadSpinner />
          </MDBRow>
        ) : (
          <>
            {this.props.popularPage.map(el => {
              if (el.list) {
                let formattedList = el.list.data.map(el => {
                  return el.playlist
                })
                return (
                  <MDBRow className={s.category} key={el.list.title}>
                    <div><h2>{el.list.title}</h2></div>
                    <PlaylistContainer playlist={formattedList} />
                  </MDBRow>
                )
              }
              return(<></>)
            })}
          </>
        )}
      </>
    )
  }
}

export default withSearchProps(PopularPage)
