import './index.css'

import React from 'react'
import { getPlaylistAudios, getPlaylistNextPage } from '../../../../store/search/actions'
import CustomPlaylist from '../../../Common/CustomPlaylist/CustomPlaylist'
import EditPlaylistModal from '../../../Common/EditPlaylistModal/EditPlaylistModal'
import withSearchProps from '../../../../containers/withSearchProps'
import {
  DEFAULT_PLAYLIST_DYNAMIC,
  DEFAULT_PLAYLIST_EXTERNAL_ID,
  DEFAULT_PLAYLIST_SOURCE,
} from '../../../../constants/AppStringConstants'

class PlaylistPage extends React.Component {
  constructor(props) {
    super(props)
    let playlist
    if (!props.isPlaylistsFetching) {
      playlist = {
        id: this.props.match.params.id,
        source:
          this.props.match.params.source !== undefined
            ? this.props.match.params.source
            : DEFAULT_PLAYLIST_SOURCE,
        external_id:
          this.props.match.params.external_id !== undefined
            ? this.props.match.params.external_id
            : DEFAULT_PLAYLIST_EXTERNAL_ID,
      }

      props.dispatch(getPlaylistAudios(playlist))
    }

    this.state = {
      currentPlaylist: playlist,
      showModal: false,
      playlist: playlist,
      dynamic:
        this.props.match.params.dynamic !== undefined
          ? this.props.match.params.dynamic === 'true'
          : DEFAULT_PLAYLIST_DYNAMIC === 'true',
    }
  }

  toggleModal = () => {
    this.setState({
      showModal: !this.state.showModal,
      playlistName: '',
      playlistUrl: '',
    })
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (
      prevProps.match.params.external_id !==
        this.props.match.params.external_id ||
      prevProps.match.params.id !== this.props.match.params.id ||
      (prevProps.isPlaylistsFetching === true &&
        this.props.isPlaylistsFetching === false) ||
      prevProps.match.path !== this.props.match.path
    ) {
      let playlist
      if (!this.props.isPlaylistsFetching) {
        playlist = {
          id: this.props.match.params.id,
          source:
            this.props.match.params.source !== undefined
              ? this.props.match.params.source
              : DEFAULT_PLAYLIST_SOURCE,
          external_id:
            this.props.match.params.external_id !== undefined
              ? this.props.match.params.external_id
              : DEFAULT_PLAYLIST_EXTERNAL_ID,
        }

        this.setState({
          playlist: playlist,
          dynamic:  this.props.match.params.dynamic !== undefined
            ? this.props.match.params.dynamic === 'true'
            : DEFAULT_PLAYLIST_DYNAMIC === 'true',
          currentPlaylist: playlist,
        })
        this.props.dispatch(getPlaylistAudios(playlist))
      }
    }
  }

  componentDidMount() {
    window.addEventListener('scroll', this.onScroll)
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.onScroll)
  }

  onScroll = () => {
    let scrollHeight = Math.max(
      document.body.scrollHeight,
      document.documentElement.scrollHeight,
      document.body.offsetHeight,
      document.documentElement.offsetHeight,
      document.body.clientHeight,
      document.documentElement.clientHeight
    )

    // Preload music
    const spacePreload = 0

    const toBottom =
      scrollHeight - window.scrollY - 1 - window.innerHeight - spacePreload

    const isNextLoad = toBottom <= 0

    if (
      isNextLoad &&
      !this.props.isFetchingNext &&
      !this.props.isFetching &&
      this.props.nextSearchUrl !== null
    ) {
      this.props.dispatch(getPlaylistNextPage(this.props.nextSearchUrl))
    }
  }

  render() {
    return (
      <>
        <EditPlaylistModal
          btnSize={'sm'}
          btnClassName={'blue-gradient editPlaylistButton'}
          showModal={this.state.showModal}
          toggleModal={this.toggleModal}
          history={this.props.history}
          match={this.props.match}
        />
        <CustomPlaylist immutable={this.state.dynamic} />
      </>
    )
  }
}

export default withSearchProps(PlaylistPage)
