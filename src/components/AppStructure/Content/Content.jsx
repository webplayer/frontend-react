import React from 'react'
import { Route, Switch } from 'react-router-dom'
import SearchPage from '../Pages/SearchPage/SearchPage'
import PlaylistPage from '../Pages/PlaylistPage/PlaylistPage'
import YoutubePlaylistPage from '../Pages/YoutubePlaylistPage/YoutubePlaylistPage'
import YoutubeChannelPage from '../Pages/YoutubeChannelPage/YoutubeChannelPage'
import { MDBContainer } from 'mdbreact'
import UserPlaylists from '../../Common/UserPlaylists/UserPlaylists'
import withSearchProps from '../../../containers/withSearchProps'

const Content = () => {
  return (
    <MDBContainer fluid className={'Playlist'}>
      {/*<VideoModule/>*/}
      <UserPlaylists />
      <Switch>
        <Route exact path={'/search/:q'} component={SearchPage} />
        <Route exact path={'/playlist/id/:id/:dynamic'} component={PlaylistPage} />
        <Route exact path={'/playlist/external/:source/:external_id/:dynamic'} component={PlaylistPage} />
        <Route
          exact
          path={'/youtube/playlist/:id'}
          component={YoutubePlaylistPage}
        />
        <Route
          exact
          path={'/youtube/channel/:id'}
          component={YoutubeChannelPage}
        />
        <Route exact path={'/'} component={PlaylistPage} />
      </Switch>
    </MDBContainer>
  )
}

export default withSearchProps(Content)
