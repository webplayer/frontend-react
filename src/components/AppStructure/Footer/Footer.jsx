import './index.css'

import React from 'react'
import Player from '../../Common/Player/Player'
import { MDBNavbar, MDBNavbarNav } from 'mdbreact'

export default function Footer() {
  return (
    <MDBNavbar
      id="custom-footer"
      fixed="bottom"
    >
      <MDBNavbarNav>
        <Player />
      </MDBNavbarNav>
    </MDBNavbar>
  )
}
