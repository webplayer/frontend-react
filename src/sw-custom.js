if ("function" === typeof importScripts) {

  importScripts(
    "https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js"
  );

  // Global workbox
  if (workbox) {

    console.log("Workbox is loaded");

    // Disable logging
    workbox.setConfig({ debug: false });


    //`generateSW` and `generateSWString` provide the option
    // to force update an exiting service worker.
    // Since we're using `injectManifest` to build SW,
    // manually overriding the skipWaiting();
    self.addEventListener("install", event => {
      self.skipWaiting();
      window.location.reload();
    });


    // Manual injection point for manifest files.
    // All assets under build/ and 5MB sizes are precached.
    workbox.precaching.precacheAndRoute(self.__WB_MANIFEST);
    workbox.precaching.precacheAndRoute([]);

    // Font caching
    workbox.routing.registerRoute(
      new RegExp("https://fonts.(?:.googlepis|gstatic).com/(.*)"),
      new workbox.strategies.CacheFirst({
        cacheName: "googleapis",
        plugins: [
          new workbox.expiration.Plugin({
            maxEntries: 30
          })
        ]
      })
    );

    // Image caching
    workbox.routing.registerRoute(
      /\.(?:png|gif|jpg|jpeg|svg)$/,
      new workbox.strategies.CacheFirst({
        cacheName: "images",
        plugins: [
          new workbox.expiration.Plugin({
            maxEntries: 60,
            maxAgeSeconds: 30 * 24 * 60 * 60 // 30 Days
          })
        ]
      })
    );

    // JS, CSS caching
    workbox.routing.registerRoute(
      /\.(?:js|css)$/,
      new workbox.strategies.StaleWhileRevalidate({
        cacheName: "static-resources",
        plugins: [
          new workbox.expiration.Plugin({
            maxEntries: 60,
            maxAgeSeconds: 20 * 24 * 60 * 60 // 20 Days
          })
        ]
      })
    );
    workbox.routing.registerRoute(
      new RegExp('^https://prodserver\\.puvel\\.ru:443/status'),
      new workbox.strategies.NetworkFirst({
        cacheName: 'status-cache',
        plugins: [
          new workbox.cacheableResponse.Plugin({
            statuses: [0, 200],
          })
        ]
      })
    );

    workbox.routing.registerRoute(
      new RegExp('^https://prodserver\\.puvel\\.ru:443/playlists/get'),
      new workbox.strategies.NetworkFirst({
        cacheName: 'playlists-cache',
        plugins: [
          new workbox.cacheableResponse.Plugin({
            statuses: [0, 200],
          })
        ]
      })
    );

    workbox.routing.registerRoute(
      new RegExp('^https://prodserver\\.puvel\\.ru:443/playlists/all'),
      new workbox.strategies.NetworkFirst({
        cacheName: 'user-playlists-cache',
        plugins: [
          new workbox.cacheableResponse.Plugin({
            statuses: [0, 200],
          })
        ]
      })
    );

    workbox.routing.registerRoute(
      new RegExp('^https://prodserver\\.puvel\\.ru:443/map'),
      new workbox.strategies.NetworkFirst({
        cacheName: 'sources-map-cache',
        plugins: [
          new workbox.cacheableResponse.Plugin({
            statuses: [0, 200],
          })
        ]
      })
    );

    workbox.routing.registerRoute(
      new RegExp('^https://prodserver\\.puvel\\.ru:443/playlists/external'),
      new workbox.strategies.NetworkFirst({
        cacheName: 'external-playlist-cache',
        plugins: [
          new workbox.cacheableResponse.Plugin({
            statuses: [0, 200],
          })
        ]
      })
    );

  } else {
    console.error("Workbox could not be loaded. No offline support.");
  }
}
