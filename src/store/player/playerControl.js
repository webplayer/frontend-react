import { setCurrentAudio, setPlaying } from './actions'
import { BACKEND_SOURCE } from '../../constants/ServerConstants'

let playlist = []
let shuffledPlaylist = []
let currentAudioId = 0
let playerRef = null
let dispatch = null
let isRandom = false
let isFirstSet = true

export const setPlaylist = (newPlaylist, audio) => {
  playlist = newPlaylist.map(music => {
    return music.audio
  })
  shuffledPlaylist = shuffle(playlist.slice())

  currentAudioId = playlist.findIndex(music => music === audio)

  isFirstSet = true
  setAudio(audio)
}

export const expandPlaylist = newPlaylist => {
  playlist.concat(
    newPlaylist.filter(music => {
      return music.audio
    })
  )
}

export const switchAudio = audio => {
  currentAudioId = playlist.findIndex(music => music === audio)
  setAudio(audio)
}

export const setPlayerRef = newPlayerRef => {
  playerRef = newPlayerRef
}

export const setDispatch = newDispatch => {
  dispatch = newDispatch
}

export const play = () => {
  if (playlist.length === 0) return
  try {
    navigator.mediaSession.playbackState = 'playing'
  } catch (e) {}
  playerRef.audioEl.play()
  dispatch(setPlaying(true))
}

export const pause = () => {
  if (playlist.length === 0) return
  try {
    navigator.mediaSession.playbackState = 'paused'
  } catch (e) {}
  playerRef.audioEl.pause()
  dispatch(setPlaying(false))
}

export const next = () => {
  if (playlist.length === 0) return
  currentAudioId = (currentAudioId + 1) % playlist.length
  if (isRandom) {
    setAudio(shuffledPlaylist[currentAudioId])
  } else {
    setAudio(playlist[currentAudioId])
  }
}

export const prev = () => {
  if (playlist.length === 0) return
  currentAudioId =
    currentAudioId === 0 ? playlist.length - 1 : currentAudioId - 1
  if (isRandom) {
    setAudio(shuffledPlaylist[currentAudioId])
  } else {
    setAudio(playlist[currentAudioId])
  }
}

export const setAudio = audio => {
  document.title = audio.title

  let thumbnail = audio.thumbnail
  if (audio.thumbnail === null) {
    thumbnail = require('../../components/Common/Playlist/thumbnail.png')
  }
  try {
    /* eslint-disable-next-line */
    navigator.mediaSession.metadata = new MediaMetadata({
      title: audio.title,
      artist: audio.artist,
      artwork: [{ src: thumbnail, sizes: '256x256', type: 'image/png' }],
    })
  } catch (e) {}

  playerRef.audioEl.src =
    BACKEND_SOURCE + '/redirect?s=' + audio.source + '&id=' + audio.id

  try {
    navigator.mediaSession.playbackState = 'playing'
  } catch (e) {}
  dispatch(setPlaying(true))

  if (isRandom) {
    if(isFirstSet){
      dispatch(setCurrentAudio(playlist[currentAudioId]))
      isFirstSet = false
    } else {
      dispatch(setCurrentAudio(shuffledPlaylist[currentAudioId]))
    }
  } else {
    dispatch(setCurrentAudio(playlist[currentAudioId]))
  }
}

export const toggleRandom = () => {
  isRandom = !isRandom
}

function shuffle(arr) {
  var j, temp
  for (var i = arr.length - 1; i > 0; i--) {
    j = Math.floor(Math.random() * (i + 1))
    temp = arr[j]
    arr[j] = arr[i]
    arr[i] = temp
  }
  return arr
}
