import { SET_CURRENT_AUDIO, SET_PLAYING } from './actions'

const initialState = {
  currentAudio: {},
  isPlaying: false,
}

export const playerReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_PLAYING: {
      return { ...state, isPlaying: action.payload }
    }
    case SET_CURRENT_AUDIO: {
      return { ...state, currentAudio: action.payload }
    }
    default: {
      return state
    }
  }
}
