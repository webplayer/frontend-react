export const SET_PLAYER_REF = 'SET_PLAYER_REF'

export const SET_PLAYLIST = 'SET_PLAYLIST'

export const EXPAND_PLAYLIST = 'EXPAND_PLAYLIST'

export const SET_PLAYING = 'SET_PLAYING'

export const SET_CURRENT_AUDIO = 'SET_CURRENT_AUDIO'

export const setPlaying = isPlaying => {
  return {
    type: SET_PLAYING,
    payload: isPlaying,
  }
}

export const setCurrentAudio = audio => {
  return {
    type: SET_CURRENT_AUDIO,
    payload: audio,
  }
}
