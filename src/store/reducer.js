import { combineReducers } from 'redux'
import { authReducer } from './auth/reducer'
import { searchReducer } from './search/reducer'
import { mobileSearchReducer } from './mobileSearch/reducer'
import { playerReducer } from './player/reducer'

export const rootReducer = combineReducers({
  authReducer: authReducer,
  playerReducer: playerReducer,
  searchReducer: searchReducer,
  mobileSearchReducer: mobileSearchReducer,
})
