import axios from 'axios'
import { BACKEND_SOURCE } from '../../constants/ServerConstants'

export const SET_PLAYLIST = 'SET_PLAYLIST'

export const PLAY = 'PLAY'

export const SEARCH_MUSIC_REQUEST = 'SEARCH_MUSIC_REQUEST'
export const SEARCH_MUSIC_SUCCESS = 'SEARCH_MUSIC_SUCCESS'

export const GET_PLAYLISTS_REQUEST = 'GET_PLAYLISTS_REQUEST'
export const GET_PLAYLISTS_SUCCESS = 'GET_PLAYLISTS_SUCCESS'

export const GET_PLAYLIST_AUDIO_REQUEST = 'GET_PLAYLIST_AUDIO_REQUEST'
export const GET_PLAYLIST_AUDIO_SUCCESS = 'GET_PLAYLIST_AUDIO_SUCCESS'
export const GET_PLAYLIST_AUDIO_ERROR = 'GET_PLAYLIST_AUDIO_ERROR'

export const SEARCH_MUSIC_NEXT_REQUEST = 'SEARCH_MUSIC_NEXT_REQUEST'
export const SEARCH_MUSIC_NEXT_SUCCESS = 'SEARCH_MUSIC_NEXT_SUCCESS'

export const SET_CURRENT_SONG_REQUEST = 'SET_CURRENT_SONG_REQUEST'
export const SET_CURRENT_SONG_SUCCESS = 'SET_CURRENT_SONG_SUCCESS'

export const GET_BUFFERED_PAGE_REQUEST = 'GET_BUFFERED_PAGE_REQUEST'
export const GET_BUFFERED_PAGE_SUCCESS = 'GET_BUFFERED_PAGE_SUCCESS'
export const GET_BUFFERED_PAGE_ERROR = 'GET_BUFFERED_PAGE_ERROR'

export const ADD_PLAYLIST_REQUEST = 'ADD_PLAYLIST_REQUEST'
export const ADD_PLAYLIST_SUCCESS = 'ADD_PLAYLIST_SUCCESS'

export const ADD_AUDIO_REQUEST = 'ADD_PLAYLIST_REQUEST'
export const ADD_AUDIO_SUCCESS = 'ADD_PLAYLIST_SUCCESS'

export const DELETE_AUDIO_REQUEST = 'DELETE_AUDIO_REQUEST'
export const DELETE_AUDIO_SUCCESS = 'DELETE_AUDIO_SUCCESS'

export const EDIT_PLAYLIST_REQUEST = 'EDIT_PLAYLIST_REQUEST'
export const EDIT_PLAYLIST_SUCCESS = 'EDIT_PLAYLIST_SUCCESS'

export const DELETE_PLAYLIST_REQUEST = 'DELETE_PLAYLIST_REQUEST'
export const DELETE_PLAYLIST_SUCCESS = 'DELETE_PLAYLIST_SUCCESS'

export const GET_POPULAR_REQUEST = 'GET_POPULAR_REQUEST'
export const GET_POPULAR_SUCCESS = 'GET_POPULAR_SUCCESS'

export const GET_SEARCH_PARAMS = 'GET_SEARCH_PARAMS'
export const TOGGLE_SEARCH_PARAMS = 'TOGGLE_SEARCH_PARAMS'

export const SET_SOURCES_STATUS = 'SET_SOURCES_STATUS'

export const GET_PLAYLIST_NEXT_PAGE = 'GET_PLAYLIST_NEXT_PAGE'

export const ADD_EXTERNAL_PLAYLIST = 'ADD_EXTERNAL_PLAYLIST'

export const play = isPLaying => {
  return {
    type: PLAY,
    payload: isPLaying,
  }
}

export const setCurrentSong = (song, isChangePlaylist) => {
  return dispatch => {
    dispatch({ type: SET_CURRENT_SONG_REQUEST })

    document.title = song.title

    let thumbnail = song.thumbnail
    if (song.thumbnail === null) {
      thumbnail = require('../../components/Common/Playlist/thumbnail.png')
    }
    try {
      /* eslint-disable-next-line */
      navigator.mediaSession.metadata = new MediaMetadata({
        title: song.title,
        artist: song.artist,
        artwork: [{ src: thumbnail, sizes: '256x256', type: 'image/png' }],
      })
    } catch (e) {}

    dispatch({
      type: SET_CURRENT_SONG_SUCCESS,
      payload: song,
      isChangePlaylist: isChangePlaylist,
    })
  }
}

export const searchMusic = (q, sources, dispatch) => {
  return dispatch => {
    dispatch({ type: SEARCH_MUSIC_REQUEST })

    let sourcesPromises = []
    for (let source of sources) {
      sourcesPromises.push(source.newSearch(q))
    }
    Promise.all(sourcesPromises).then(function() {
      let playlist = [].concat.apply([], ...arguments)
      dispatch({
        type: SEARCH_MUSIC_SUCCESS,
        payload: {
          playlist: playlist,
          search: q,
        },
      })

      // dispatch(searchMusicNext(sources, true))
    })
  }
}

export const searchMusicNext = (sources, flag = false) => {
  return dispatch => {
    let requestType = flag
      ? GET_BUFFERED_PAGE_REQUEST
      : SEARCH_MUSIC_NEXT_REQUEST
    dispatch({ type: requestType })

    let nextSourcesPromises = []
    for (let source of sources) {
      nextSourcesPromises.push(source.getData())
    }
    Promise.all(nextSourcesPromises).then(function() {
      let requestSuccessType = flag
        ? GET_BUFFERED_PAGE_SUCCESS
        : SEARCH_MUSIC_NEXT_SUCCESS
      let nextData = [].concat.apply([], ...arguments)
      dispatch({
        type: requestSuccessType,
        payload: {
          playlist: nextData,
        },
      })
    })
  }
}

export const getPlaylists = () => {
  return dispatch => {
    dispatch({ type: GET_PLAYLISTS_REQUEST })

    axios
      .get(`${BACKEND_SOURCE}/playlists/all`, {
        withCredentials: true,
      })
      .then(res => {
        let payload = res.data
        if (res.data === '') {
          payload = []
        }
        dispatch({
          type: GET_PLAYLISTS_SUCCESS,
          payload: { playlists: payload },
        })
      })
  }
}

export const getPlaylistAudios = playlist => {
  return dispatch => {
    let URL = '/playlists'
    let payload = undefined
    if (typeof playlist == 'string') {
      URL += '/get?id=' + playlist
      payload = {id: playlist}
    } else {
      if (playlist.id !== undefined) {
        URL += '/get?id=' + playlist.id
        payload = playlist
      } else {
        payload = {id: playlist.external_id}
        URL +=
          '/external?external_id=' +
          playlist.external_id +
          '&s=' +
          playlist.source
      }
    }

    dispatch({ type: GET_PLAYLIST_AUDIO_REQUEST, payload: payload })
    searchAudio(`${BACKEND_SOURCE}${URL}`, dispatch)
  }
}

export const getExternalAudio = (source, id, externalId) => {
  return dispatch => {
    dispatch({ type: GET_PLAYLIST_AUDIO_REQUEST, payload: id })
    let url = ''
    switch (source) {
      case 'youtube/playlist':
        url = `${BACKEND_SOURCE}/youtube/playlist/audios?id=${id}`
        break
      case 'youtube/channel':
        url = `${BACKEND_SOURCE}/youtube/channel/audios?id=${id}`
        break
      default:
        break
    }
    if (url !== '') {
      searchAudio(url, dispatch)
    }
  }
}

const searchAudio = (url, dispatch) => {
  axios
    .get(encodeURI(url), {
      withCredentials: true,
    })
    .then(res => {
      let payload = res.data.data
      if (res.data.data === '') {
        payload = []
      }
      document.body.scrollTop = document.documentElement.scrollTop = 0
      dispatch({
        type: GET_PLAYLIST_AUDIO_SUCCESS,
        payload: { playlist: payload, nextSearchUrl: res.data.next },
      })
    })
    .catch(() => {
      dispatch({
        type: GET_PLAYLIST_AUDIO_ERROR,
        payload: { playlist: [] },
      })
    })
}

export const addPlaylist = (name, url) => {
  return dispatch => {
    dispatch({ type: ADD_PLAYLIST_REQUEST })

    axios(
      `${BACKEND_SOURCE}/playlists/create/?title=${name}&color=${url}&thumbnail=${url}`,
      {
        method: 'post',
        withCredentials: true,
      }
    ).then(() => {
      dispatch({
        type: ADD_PLAYLIST_SUCCESS,
      })
      dispatch(getPlaylists())
    })
  }
}

export const editPlaylist = (id, name, url) => {
  return dispatch => {
    if (name !== '' || url !== '') {
      let params = `title=${name}&color=${url}&thumbnail=${url}`
      if (name === '') {
        params = `color=${url}&thumbnail=${url}`
      } else if (url === '') {
        params = `title=${name}`
      }

      dispatch({ type: EDIT_PLAYLIST_REQUEST })
      axios(`${BACKEND_SOURCE}/playlists/update?id=${id}&${params}`, {
        method: 'POST',
        withCredentials: true,
      }).then(() => {
        dispatch({
          type: EDIT_PLAYLIST_SUCCESS,
        })
        dispatch(getPlaylists())
      })
    }
  }
}

export const deletePlaylist = id => {
  return dispatch => {
    dispatch({ type: DELETE_PLAYLIST_REQUEST })

    axios(`${BACKEND_SOURCE}/playlists/delete?id=${id}`, {
      method: 'POST',
      withCredentials: true,
    }).then(() => {
      dispatch({
        type: DELETE_PLAYLIST_SUCCESS,
      })
      dispatch(getPlaylists())
    })
  }
}

export const addAudio = (playlistId, audioId) => {
  const audioParams = audioId.split('_', 1)[0]
  audioId = audioId.replace(audioParams + '_', '')

  return dispatch => {
    dispatch({ type: ADD_AUDIO_REQUEST })

    axios(
      `${BACKEND_SOURCE}/playlists/add/?id=${playlistId}&s=${audioParams}&audio_id=${audioId}`,
      {
        method: 'POST',
        withCredentials: true,
      }
    ).then(() => {
      dispatch({
        type: ADD_AUDIO_SUCCESS,
      })
    })
  }
}

export const deleteAudio = (playlistId, audioId) => {
  return dispatch => {
    dispatch({ type: DELETE_AUDIO_REQUEST })

    axios(
      `${BACKEND_SOURCE}/playlists/remove/?id=${playlistId}&audio=${audioId}`,
      {
        method: 'POST',
        withCredentials: true,
      }
    ).then(() => {
      dispatch({
        type: DELETE_AUDIO_SUCCESS,
      })
      dispatch(getPlaylistAudios(playlistId))
    })
  }
}

export const getPopularPage = () => {
  return dispatch => {
    dispatch({ type: GET_POPULAR_REQUEST })

    axios(`${BACKEND_SOURCE}/main`, {
      method: 'GET',
      withCredentials: true,
    }).then(res => {
      dispatch({
        type: GET_POPULAR_SUCCESS,
        payload: res.data,
      })
    })
  }
}

export const getSearchParams = () => {
  return dispatch => {
    axios(`${BACKEND_SOURCE}/map`, {
      method: 'GET',
      withCredentials: true,
    }).then(res => {
      dispatch({
        type: GET_SEARCH_PARAMS,
        payload: res.data.endpoints.search,
      })
    })
  }
}

export const toggleSearchParams = () => {
  return {
    type: TOGGLE_SEARCH_PARAMS,
  }
}

export const setSourcesStatus = source => {
  return {
    type: SET_SOURCES_STATUS,
    payload: source,
  }
}

export const getPlaylistNextPage = nextSearchUrl => {
  return dispatch => {
    axios(`${BACKEND_SOURCE}${nextSearchUrl}`, {
      method: 'GET',
      withCredentials: true,
    }).then(res => {
      dispatch({
        type: GET_PLAYLIST_NEXT_PAGE,
        payload: { playlist: res.data.data, nextSearchUrl: res.data.next },
      })
    })
  }
}

export const addExternalPlaylist = (external_id, s, title, thumbnail) => {
  return dispatch => {
    axios(
      `${BACKEND_SOURCE}/playlists/external?external_id=${external_id}&s=${s}&title=${encodeURI(
        title
      )}`,
      {
        method: 'POST',
        withCredentials: true,
      }
    ).then(res => {
      dispatch({
        type: ADD_EXTERNAL_PLAYLIST,
      })
      axios
        .get(`${BACKEND_SOURCE}/playlists/all`, {
          withCredentials: true,
        })
        .then(res => {
          let targetPlaylist = res.data.find(el => el.title === title)

          dispatch({ type: EDIT_PLAYLIST_REQUEST })
          axios
            .get(`${BACKEND_SOURCE}/playlists/all`, {
              withCredentials: true,
            })
            .then(res => {
              axios(`${BACKEND_SOURCE}/playlists/update`, {
                method: 'POST',
                withCredentials: true,
                params: {
                  id: targetPlaylist.id,
                  thumbnail: thumbnail,
                },
              }).then(() => {
                dispatch(getPlaylists())
              })
            })
        })
    })
  }
}
