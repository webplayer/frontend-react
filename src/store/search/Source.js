import axios from 'axios'
import { BACKEND_SOURCE } from '../../constants/ServerConstants'

export default class Source {
  constructor(source, parameters) {
    this.status = true
    this.currentStatus = this.status
    this.source = source
    this.nextUrl = null
    this.parameters = parameters
    this.promise = null
    this.isNextSearch = false
    this.isPromiseReady = false
  }

  newSearch(q) {
    this.currentStatus = this.status
    this.nextUrl = null
    if (!this.currentStatus) {
      return new Promise(function(resolve, reject) {
        resolve([])
      })
    }
    return new Promise(
      function(resolve, reject) {
        axios
          .get(
            `${BACKEND_SOURCE}/search?q=${encodeURIComponent(q)}&s=${
              this.source
            }`,
            {
              withCredentials: true,
            }
          )
          .then(
            function(res) {
              this.nextUrl = res.data.next
              // this.promise = this.promiseNextPage()
              resolve(res.data.data)
            }.bind(this)
          ).catch(function(res) {
            this.nextUrl = ''
            resolve([])
          }.bind(this))
      }.bind(this)
    )
  }

  getData() {
    return new Promise(
      function(resolve, reject) {
        if (this.isPromiseReady) {
          let page = []
          this.promise.then(res => {
            page = res
            this.promise = this.promiseNextPage()
            resolve(page)
          })

        } else {
          if (!this.isNextSearch) {
            this.promise = this.promiseNextPage()
          }
          Promise.race([this.promise, this.startTimer()]).then(
            function(res) {
              if (res.length > 0) {
                this.promise = this.promiseNextPage()
              }
              resolve(res)
            }.bind(this)
          )
        }
      }.bind(this)
    )
  }

  startTimer() {
    return new Promise(function(resolve) {
      setTimeout(() => {
        resolve([])
      }, 1500)
    })
  }
  promiseNextPage() {
    this.isNextSearch = true
    this.isPromiseReady = false
    return new Promise(
      function(resolve, reject) {
        if (this.nextUrl === null) {
          this.isNextSearch = false
          this.isPromiseReady = true
          resolve([])
        } else {
          axios
            .get(`${BACKEND_SOURCE}${this.nextUrl}`, {
              withCredentials: true,
            })
            .then(
              function(res) {
                this.nextUrl = res.data.next
                this.isNextSearch = false
                this.isPromiseReady = true
                resolve(res.data.data)
              }.bind(this),
            )
        }
      }.bind(this)
    )
  }
}
