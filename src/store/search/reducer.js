import {
  ADD_AUDIO_REQUEST,
  ADD_AUDIO_SUCCESS,
  ADD_EXTERNAL_PLAYLIST,
  ADD_PLAYLIST_REQUEST,
  ADD_PLAYLIST_SUCCESS,
  DELETE_AUDIO_REQUEST,
  DELETE_AUDIO_SUCCESS,
  DELETE_PLAYLIST_REQUEST,
  DELETE_PLAYLIST_SUCCESS,
  GET_BUFFERED_PAGE_ERROR,
  GET_BUFFERED_PAGE_REQUEST,
  GET_BUFFERED_PAGE_SUCCESS,
  GET_PLAYLIST_AUDIO_ERROR,
  GET_PLAYLIST_AUDIO_REQUEST,
  GET_PLAYLIST_AUDIO_SUCCESS,
  GET_PLAYLIST_NEXT_PAGE,
  GET_PLAYLISTS_REQUEST,
  GET_PLAYLISTS_SUCCESS,
  GET_POPULAR_REQUEST,
  GET_POPULAR_SUCCESS,
  GET_SEARCH_PARAMS,
  PLAY,
  SEARCH_MUSIC_NEXT_REQUEST,
  SEARCH_MUSIC_NEXT_SUCCESS,
  SEARCH_MUSIC_REQUEST,
  SEARCH_MUSIC_SUCCESS,
  SET_CURRENT_SONG_REQUEST,
  SET_CURRENT_SONG_SUCCESS,
  SET_PLAYLIST,
  SET_SOURCES_STATUS,
  TOGGLE_SEARCH_PARAMS,
} from './actions'
import { DEFAULT_SEARCH } from '../../constants/AppStringConstants'

import Source from './Source'

const initialState = {
  userPlaylists: [],
  currentPlaylist: [],
  tempPlaylist: [],
  bufferedPage: [],
  popularPage: [],
  currentSong: {},
  isFetching: false,
  isFetchingNext: false,
  isPlaylistsFetching: false,
  currentSearch: DEFAULT_SEARCH,
  isPlaying: false,
  currentPlaylistId: null,
  nextSearchUrl: [],
  isFetchingCurrentSong: false,
  searchParams: [],
  isSearchParams: false,
  sources: undefined,
}

export const searchReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_PLAYLIST:
      return { ...state, tempPlaylist: action.payload }
    case PLAY:
      return { ...state, isPlaying: action.payload }
    case SEARCH_MUSIC_REQUEST: {
      return { ...state, isFetching: true, currentSearchPage: 2 }
    }
    case SEARCH_MUSIC_SUCCESS: {
      let tempPlaylist = [
        ...new Set(action.payload.playlist.map(JSON.stringify)),
      ].map(JSON.parse)
      return {
        ...state,
        tempPlaylist: tempPlaylist,
        currentSearch: action.payload.search,
        nextSearchUrl: action.payload.nextSearchUrl,
        isFetching: false,
      }
    }
    case GET_PLAYLISTS_REQUEST: {
      return { ...state, isPlaylistsFetching: true }
    }
    case GET_PLAYLISTS_SUCCESS:
      return {
        ...state,
        userPlaylists: action.payload.playlists,
        isPlaylistsFetching: false,
      }
    case SEARCH_MUSIC_NEXT_REQUEST: {
      let united = [...state.tempPlaylist, ...state.bufferedPage]
      let tempPlaylist = [...new Set(united.map(JSON.stringify))].map(
        JSON.parse
      )
      return {
        ...state,
        isFetchingNext: true,
        tempPlaylist: tempPlaylist,
      }
    }
    case SEARCH_MUSIC_NEXT_SUCCESS:
      return {
        ...state,
        bufferedPage: action.payload.playlist,
        nextSearchUrl: action.payload.nextSearchUrl,
        isFetchingNext: false,
      }
    case GET_BUFFERED_PAGE_REQUEST:
      return {
        ...state,
        isFetchingNext: true,
      }
    case GET_BUFFERED_PAGE_SUCCESS:
      return {
        ...state,
        bufferedPage: action.payload.playlist,
        nextSearchUrl: action.payload.nextSearchUrl,
        isFetchingNext: false,
      }
    case GET_BUFFERED_PAGE_ERROR:
      return state
    case SET_CURRENT_SONG_REQUEST:
      return { ...state, isFetchingCurrentSong: true }
    case SET_CURRENT_SONG_SUCCESS: {
      let playlist = state.tempPlaylist
      let songIdx = playlist.indexOf(action.oldSong)
      playlist[songIdx] = action.payload
      if (!action.isChangePlaylist) {
        playlist = state.currentPlaylist
      }
      return {
        ...state,
        currentSong: action.payload,
        isPlaying: true,
        currentPlaylist: playlist,
        isFetchingCurrentSong: false,
      }
    }
    case GET_PLAYLIST_AUDIO_REQUEST:
      return {
        ...state,
        isFetching: true,
        currentPlaylistId: action.payload.id,
        bufferedPage: [],
        nextSearchUrl: null,
      }
    case GET_PLAYLIST_AUDIO_SUCCESS: {
      let tempPlaylist = [
        ...new Set(action.payload.playlist.map(JSON.stringify)),
      ].map(JSON.parse)

      return {
        ...state,
        tempPlaylist: tempPlaylist,
        isFetching: false,
        nextSearchUrl: action.payload.nextSearchUrl,
      }
    }
    case GET_PLAYLIST_AUDIO_ERROR:
      return {
        ...state,
        isFetching: false,
        tempPlaylist: [],
        nextSearchUrl: null,
      }
    case DELETE_PLAYLIST_REQUEST:
      return { ...state }
    case DELETE_PLAYLIST_SUCCESS: {
      return { ...state, currentPlaylistId: null }
    }
    case ADD_PLAYLIST_REQUEST: {
      return state
    }
    case ADD_PLAYLIST_SUCCESS: {
      return state
    }
    case ADD_AUDIO_REQUEST: {
      return state
    }
    case ADD_AUDIO_SUCCESS: {
      return state
    }
    case DELETE_AUDIO_REQUEST: {
      return state
    }
    case DELETE_AUDIO_SUCCESS: {
      return state
    }
    case GET_POPULAR_REQUEST: {
      return { ...state, isFetching: true }
    }
    case GET_POPULAR_SUCCESS: {
      return { ...state, isFetching: false, popularPage: action.payload.data }
    }
    case GET_SEARCH_PARAMS: {
      let sources = action.payload.map(el => {
        return new Source(el.source, el.params)
      })
      return { ...state, searchParams: action.payload, sources: sources }
    }
    case TOGGLE_SEARCH_PARAMS: {
      return { ...state, isSearchParams: !state.isSearchParams }
    }
    case SET_SOURCES_STATUS: {

      action.payload.status = !action.payload.status
      let sources = state.sources

      return { ...state,sources : sources }
    }
    case GET_PLAYLIST_NEXT_PAGE: {
      let united = [...state.tempPlaylist, ...action.payload.playlist]
      let tempPlaylist = [...new Set(united.map(JSON.stringify))].map(
        JSON.parse
      )
      return { ...state, tempPlaylist:tempPlaylist, nextSearchUrl: action.payload.nextSearchUrl}
    }
    case ADD_EXTERNAL_PLAYLIST:{
      return state
    }
    default:
      return state
  }
}
