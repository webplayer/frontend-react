export const SET_MOBILE_SEARCH = 'SET_MOBILE_SEARCH'

export const SEARCH_SIGNAL = 'SEARCH_SIGNAL'

export const setMobileSearch = bool => {
  return {
    type: SET_MOBILE_SEARCH,
    payload: bool && window.innerWidth < 768,
  }
}

export const searchSignal = () => {
  return {
    type: SEARCH_SIGNAL,
  }
}
