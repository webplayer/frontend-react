import { SEARCH_SIGNAL, SET_MOBILE_SEARCH } from './actions'

const initialState = {
  mobileSearch: false,
  signal: false,
}

export const mobileSearchReducer = (state = initialState, action) => {
  switch(action.type){
    case SET_MOBILE_SEARCH:
    {
      return {
        ...state,
        mobileSearch: action.payload,
      }
    }
    case SEARCH_SIGNAL:{
      return {
        ...state,
        signal: !state.signal,
      }
    }
    default:{
      return state
    }
  }

}
