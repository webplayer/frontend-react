// import 'bootstrap/dist/css/bootstrap.min.css';
import { applyMiddleware, createStore } from 'redux'
import { rootReducer } from './reducer'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'
import { logger } from 'redux-logger/src'

export const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk,logger)))
