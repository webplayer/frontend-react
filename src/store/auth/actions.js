import axios from 'axios'
import * as qs from 'qs'
import { BACKEND_SOURCE } from '../../constants/ServerConstants'
import { getPlaylists } from '../search/actions'

export const APP_SIGN_UP_REQUEST = 'APP_SIGN_UP_REQUEST'
export const APP_SIGN_UP_SUCCESS = 'APP_SIGN_UP_SUCCESS'

export const APP_SIGN_IN_REQUEST = 'APP_SIGN_IN_REQUEST'
export const APP_SIGN_IN_SUCCESS = 'APP_SIGN_IN_SUCCESS'
export const APP_SIGN_IN_ERROR = 'APP_SIGN_IN_ERROR'

export const VK_SIGN_IN_REQUEST = 'VK_SIGN_IN_REQUEST'
export const VK_SIGN_IN_SUCCESS = 'VK_SIGN_IN_SUCCESS'
export const VK_SIGN_IN_ERROR = 'VK_SIGN_IN_ERROR'

export const SIGN_OUT_REQUEST = 'SIGN_OUT_REQUEST'
export const SIGN_OUT_SUCCESS = 'SIGN_OUT_SUCCESS'

export const CHECK_AUTH_REQUEST = 'CHECK_AUTH_REQUEST'
export const CHECK_AUTH_SUCCESS = 'CHECK_AUTH_SUCCESS'
export const CHECK_AUTH_ERROR = 'CHECK_AUTH_ERROR'

export const SET_BACKGROUND_IMAGE = 'SET_BACKGROUND_IMAGE'

export const signUp = (username, password) => {
  return dispatch => {
    dispatch({ type: APP_SIGN_UP_REQUEST })

    const user = {
      username: username,
      password: password,
    }

    axios(
      `${BACKEND_SOURCE}/register?${qs.stringify(user)}`,
      {
        method: 'POST',
        withCredentials: true,
      }
    ).then(()=>{
      dispatch({
        type: APP_SIGN_UP_SUCCESS,
        payload: user.username,
      })
      dispatch(appSignIn(username, password))
    })
  }
}

export const appSignIn = (username, password) => {
  return dispatch => {
    dispatch({ type: APP_SIGN_IN_REQUEST })

    const user = {
      username: username,
      password: password,
    }

    axios
      .get(`${BACKEND_SOURCE}/login?${qs.stringify(user)}`, {
        withCredentials: true,
      })
      .then(res => {
        if (res != null) {
          dispatch({
            type: APP_SIGN_IN_SUCCESS,
            payload: user.username,
          })
          dispatch(checkAuthRequest())
          dispatch(getPlaylists())
        }
      })
      .catch(e => {
        if(e.response.data.error) {
          dispatch({
            type: APP_SIGN_IN_ERROR,
            payload: e.response.data.error,
          })
        }
      })
  }
}

export const vkSignIn = (username, password) => {
  return dispatch => {
    dispatch({ type: VK_SIGN_IN_REQUEST })

    const user = {
      force: true,
      username: username,
      password: password,
      s: 'vk',
    }

    axios(
      `${BACKEND_SOURCE}/auth?${qs.stringify(user)}`,
      {
        method: 'POST',
        withCredentials: true,
      }
    ).then(res => {
      if (res != null) {
        dispatch({
          type: VK_SIGN_IN_SUCCESS,
        })
      }
      dispatch(checkAuthRequest())
    })
      .catch(() => {
        dispatch({
          type: VK_SIGN_IN_ERROR,
        })
        dispatch(checkAuthRequest())
      })
  }
}

export const appSignOut = ()=>{
  return dispatch => {
    dispatch({ type: SIGN_OUT_REQUEST })

    axios(
      `${BACKEND_SOURCE}/login`,
      {
        method: 'DELETE',
        withCredentials: true,
      }
    ).then(()=>{
      dispatch({
        type: SIGN_OUT_SUCCESS,
      })
      // dispatch(checkAuthRequest())
    })
  }
}

export const setBackgroundImage = url => {
  localStorage.setItem('backgroundImage', url)
  return {
    type: SET_BACKGROUND_IMAGE,
    payload: url,
  }
}

export const checkAuthRequest = () => {
  return dispatch => {
    dispatch({ type: CHECK_AUTH_REQUEST })

    axios
      .get(`${BACKEND_SOURCE}/status`, {
        withCredentials: true,
      })
      .then(res => {
        if (res !== null) {
          dispatch({
            type: CHECK_AUTH_SUCCESS,
            payload: {
              isApp: res.data.status.youtube,
              isVK: res.data.status.vk,
              username: res.data.username,
            },
          })
        }
      }).catch(() =>{
      dispatch({
        type: CHECK_AUTH_ERROR,
        payload: {
          isApp: false,
          isVK: false,
          username: '',
        },
      })
    })
  }
}
