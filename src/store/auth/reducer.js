import {
  APP_SIGN_IN_ERROR,
  APP_SIGN_IN_REQUEST,
  APP_SIGN_IN_SUCCESS,
  APP_SIGN_UP_REQUEST,
  APP_SIGN_UP_SUCCESS,
  CHECK_AUTH_ERROR,
  CHECK_AUTH_REQUEST,
  CHECK_AUTH_SUCCESS,
  SET_BACKGROUND_IMAGE,
  SIGN_OUT_REQUEST,
  SIGN_OUT_SUCCESS,
  VK_SIGN_IN_ERROR,
  VK_SIGN_IN_REQUEST,
  VK_SIGN_IN_SUCCESS,
} from './actions'

const initialState = {
  isAppSignIn: false,
  isAppSignUpFetching: false,
  isAppSignInFetching: false,
  isVkSignIn: false,
  isVkSignInFetching: false,
  username: '',
  backgroundImage: '',
  errorResponse: '',
}

export const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case APP_SIGN_UP_REQUEST:
      return {
        ...state,
        isAppSignUpFetching: true,
      }
    case APP_SIGN_UP_SUCCESS:
      return {
        ...state,
        username: action.payload,
        isAppSignIn: true,
        isAppSignUpFetching: false,
      }
    case APP_SIGN_IN_REQUEST:
      return {
        ...state,
        isAppSignInFetching: true,
        errorResponse: '',
      }
    case APP_SIGN_IN_SUCCESS:
      return {
        ...state,
        username: action.payload,
        isAppSignIn: true,
        isAppSignInFetching: false,
      }
    case APP_SIGN_IN_ERROR:
      return {
        ...state,
        isAppSignIn: false,
        isAppSignInFetching: false,
        errorResponse: action.payload,
      }
    case VK_SIGN_IN_REQUEST:
      return {
        ...state,
        isVkSignInFetching: true,
      }
    case VK_SIGN_IN_SUCCESS:
      return {
        ...state,
        isVkSignIn: true,
        isVkSignInFetching: false,
      }
    case VK_SIGN_IN_ERROR:
      return {
        ...state,
        isVkSignIn: false,
        isVkSignInFetching: false,
      }
    case SET_BACKGROUND_IMAGE:
      return {
        ...state,
        backgroundImage: action.payload,
      }
    case CHECK_AUTH_REQUEST:
      return {
        ...state,
      }
    case CHECK_AUTH_SUCCESS:
      return {
        ...state,
        isAppSignIn: action.payload.isApp,
        isVkSignIn: action.payload.isVK,
        username: action.payload.username,
      }
    case CHECK_AUTH_ERROR:
      return {
        ...state,
        isAppSignIn: action.payload.isApp,
        isVkSignIn: action.payload.isVK,
        username: action.payload.username,
      }
    case SIGN_OUT_REQUEST:
      return {
        ...state,
      }
    case SIGN_OUT_SUCCESS:
      return {
        ...state,
        isAppSignIn: false,
        isVkSignIn: false,
        username: '',
      }
    default:
      return state
  }
}
